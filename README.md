# Projet Reseaux Master 1 Artois 2018-2019

### Brandao Arthur & Maxence Bacquet



## Utilisation

Pour lancer le serveur il suffit de le compiler avec `make` (il est possible de recréer les dépendances avec `make depend`). Pour lancer le serveur il suffit d'exécuter le fichier main. Pour couper le serveur Ctrl+C.

Pour lancer le client il suffit d'exécuter le fichier start.sh. Pour la compilation il faut inclure la bibliothèque json.jar présente dans le dossier lib. De même pour la bonne exécution de Client.jar (lancé par start.sh) le dossier lib contenant la bibliothèque json.jar doit être dans le même dossier je l'archive java.



## Commandes

**Menu**

- Z : Menu précédent
- S : Menu suivant
- Entrée : Valider

**Jeu**

- Z : Vers le haut
- Q : Vers la gauche
- S : Vers le bas
- D : Vers la droite
- I : Regarder vers le haut
- J : Regarder vers la droites
- K : Regarder vers la bas
- L : Regarder vers la droite
- A : Poser une bombe
- E : Poser une mine
- R : Poser une remote
- F : Activer remote
- Echap : Retour vers le menu

Le BomberStudent pose les bombes vers la où il regarde



## Fonctionnement réseaux

Pour le réseau notre projet utilise un serveur UDP et deux serveurs TCP. Le serveur UDP permet le broadcast pour repérer les serveurs disponibles. Le 1er serveur TCP permet la communication entre le client et le serveur, c'est par celui-ci que passent toutes les requêtes du client au serveur (exemple : déplacement du personnage), et c'est par cette connexion que le serveur y répond (exemple : déplacement autorisé ou non). La seconde connexion permet la communication entre le serveur et le client, il est utilisé par le serveur pour notifier un événement au client (exemple: explosion d'une bombe), le serveur n'attend pas de réponse du client.



## Fonctionnement serveur

Le serveur possède 3 Threads (le principal, le serveur UDP, le serveur TCP) plus un Thread par client. Le serveur attend la réception d'une requête client, la parse et regarde s'il posséde un handler pour l'action demandée. S'il y en a un alors il l'exécute sinon il renvoie un message d'erreur. Le serveur possède aussi une fonction delay qui va créer un thread attendre X secondes et appeler un callback, elle est utilisée pour gérer le temps d'explosion des bombes et de l'invicibilité.



## Fonctionnement client

Le client utilise 3 Threads, le thread principal qui lance le jeu, le thread d'attente de notification du serveur (attend en boucle la prochaine notification du serveur) et le thread d'affichage (qui utilise JavaFX). L'affichage est fait en utilisant la webview de JavaFX pour afficher du HTML. Le réseau est géré par la class BomberStudentClient qui s'occupe de formatter les requêtes et d'utiliser le client TCP. Pour les notifications du serveur c'est la class BomberStudentRequest (qui est lancée dans un thread par BomberStudentClient) qui les réceptionne et qui regarde si le client à un handler associé à appeler. Il est possible de changer les sprites en modifiant les images dans le dossier file/sprite (deux lots de sprites sont fournis avec le projet, ceux de bomberman et une version simpliste utilisée pour nos tests).



## Bugs connus

**Client**

- Rarement lors du ramassage d'un objet le client crash
- Lors d'un game over suite à la pression d'une touche pour revenir dans le menu l'ecran de jeu se reaffiche, il est necessaire de bouger dans le menu (Z/S) pour actualiser l'affichage et voir le menu.
- Lorsque l'on selectionne rejoindre alors qu'il n'exesite aucune partie l'ecran devient blanc, il est necessaire de bouger dans le menu (Z/S) pour actualiser l'affichage et revoir le menu.