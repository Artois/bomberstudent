/* 
 * File:   delay.h
 * Author: Arthur Brandao
 *
 * Created on 4 décembre 2018
 */

#ifndef DELAY_H
#define DELAY_H

/* --- Include --- */
#include "constante.h"
#include "game.h"

/* --- Structure --- */
typedef struct{
    int second; //Seconde à attendre
    int game; //Index de la game dans le tableau
    int player; //Index du joueur dans le tableau de game
    void* data; //Autres données pour le callback
    int(*callback)(Game*, int, void*);
}delay_t;

/* --- Fonctions --- */
/**
 * Attend X secondes (sans bloquer l'execution) avant d'executer le callback
 * @param int Nombre de second en attente
 * @param int Index de la game
 * @param int Index du joueur dans la game
 * @param int(*)(Game*, int) Le callback
 */
void delay(int, int, int, int(*)(Game*, int, void*));

/**
 * Attend X secondes (sans bloquer l'execution) avant d'executer le callback
 * @param int Nombre de second en attente
 * @param int Index de la game
 * @param int Index du joueur dans la game
 * @param void* Paramétre suplémentaire pour le callback
 * @param int(*)(Game*, int) Le callback
 */
void delay_data(int, int, int, void*, int(*)(Game*, int, void*));

/**
 * Fin du bonus major
 * @param Game* La game du joueur
 * @param int L'index de joueur
 * @param void* D'autre données
 * @return Réussite
 */
int callback_major_end(Game*, int, void*);

/**
 * Explosion d'une bombe
 * @param Game* La game du joueur
 * @param int L'index de joueur
 * @param void* D'autre données
 * @return Réussite
 */
int callback_bomb_explode(Game*, int, void*);

#endif /* DELAY_H */

