/* 
 * File:   json.h
 * Author: Arthur Brandao
 *
 * Created on 28 octobre 2018
 */
#ifndef JSON_H
#define JSON_H

/* --- Include --- */
#include "str.h"
#include "constante.h"

/* --- Constante --- */
#define JSON_MAX_SIZE 8192
#define JSON_ERROR -1
#define JSON_OK 0
#define JSON_STRING 1
#define JSON_NUMBER 2
#define JSON_BOOLEAN 3
#define JSON_ARRAY 4
#define JSON_OBJECT 5
#define JSON_ARRAY_PARSER 0
#define JSON_ARRAY_ENCODER 1

/* --- Structure --- */
typedef struct JsonNode JsonNode;

typedef struct{
    char* str; //La chaine de carac json
    int elt; //Le nombre d'element
    char** key; //La position des clef dans la chaine
    char** val; //La position de la valeur 
    int* key_length; //La taille des clefs
    int* val_length; //La taille des valeurs
    int* type; //Le type des valeurs
}JsonParser;

typedef struct{
    char json[JSON_MAX_SIZE];
    int max;
    int pos;
}JsonEncoder;

typedef struct{
    char* str; //La chaine
    char** val; //Pointeur vers les valeurs du tableau
    int* val_length; //Taille des valeurs
    int* type; //Type de la valeur
    int length; //Taille du tableau
}JsonArrayParser;

typedef struct{
    char json[JSON_MAX_SIZE];
    int max;
    int pos;
}JsonArrayEncoder;

typedef struct{
    int mode; //Le mode (Parse ou Encode)
    JsonArrayParser* parser;
    JsonArrayEncoder* encoder;
}JsonArray;

/* --- Fonctions ---- */

//JsonParser

/**
 * Passe un objet json dans une chaine json
 * @param char* La chaine json
 * @return int Le nombre de caractères à passer
 */
int skip_object(char*);

/**
 * Passe un tableau json dans une chaine json
 * @param char* La chaine json
 * @return int Le nombre de caractères à passer
 */
int skip_array(char*);

/**
 * Parse une chaine JSON en JsonParser
 * Initialise ou écrase les données du JsonParser
 * @param JsonParser* La structure de resultat (doit être alloué)
 * @param char* La chaine en JSON
 * @return int JSON_ERROR en cas d'erreur sinon JSON_OK
 */
int json_parse(JsonParser*, char*);

/**
 * Donne la clef à un index donné
 * @param JsonParser* Le resultat de json_parse
 * @param int La position à regarder
 * @return char* La clef
 */
char* key_index(JsonParser*, int);

/**
 * Donne la valeur à un index donné
 * @param JsonParser* Le resultat de json_parse
 * @param int La position à regarder
 * @return char* La valeur
 */
char* get_index(JsonParser*, int);

/**
 * Donne la positon d'une valeur dans le JsonParser
 * @param JsonParser* Le resultat de json_parse
 * @param char* La clef lié à la valeur
 * @return int La position
 */
int get_pos(JsonParser*, char*);

/**
 * Retourne le type de la valeur
 * @param JsonParser* Le resultat de json_parse
 * @param char* La clef lié à la valeur
 * @return int constante JSON_XXXX du type de la valeur
 */
int get_type(JsonParser*, char*);

/**
 * Retourne la valeur d'un element
 * @param JsonParser* Le resultat de json_parse
 * @param char* La clef lié à la valeur
 * @return char* La valeur, NULL en cas d'erreur (clef introuvable)
 */
char* get_value(JsonParser*, char*);

/**
 * Retourne la valeur d'un element de type string
 * @param JsonParser* Le resultat de json_parse
 * @param char* La clef lié à la valeur
 * @return char* La valeur, NULL en cas d'erreur (type incorrect, clef introuvable)
 */
char* get_string(JsonParser*, char*);

/**
 * Retourne la valeur d'un element de type number
 * @param JsonParser* Le resultat de json_parse
 * @param char* La clef lié à la valeur
 * @return double La valeur, JSON_ERROR en cas d'erreur (type incorrect, clef introuvable)
 */
double get_number(JsonParser*, char*);

/**
 * Retourne la valeur d'un element de type number
 * @param JsonParser* Le resultat de json_parse
 * @param char* La clef lié à la valeur
 * @return int La valeur, JSON_ERROR en cas d'erreur (type incorrect, clef introuvable)
 */
int get_integer(JsonParser*, char*);

/**
 * Retourne la valeur d'un element de type boolean
 * @param JsonParser* Le resultat de json_parse
 * @param char* La clef lié à la valeur
 * @return boolean La valeur, false en cas d'erreur (type incorrect, clef introuvable)
 */
boolean get_boolean(JsonParser*, char*);

/**
 * Retourne la valeur d'un element de type array
 * @param JsonParser* Le resultat de json_parse
 * @param char* La clef lié à la valeur
 * @return JsonArray* La valeur, NULL en cas d'erreur (type incorrect, clef introuvable)
 */
JsonArray* get_array(JsonParser*, char*);

/**
 * Retourne la valeur d'un element de type object
 * @param JsonParser* Le resultat de json_parse
 * @param char* La clef lié à la valeur
 * @return JsonParser* L'object json analysé, NULL en cas d'erreur (type incorrect, clef introuvable, ...)
 */
JsonParser* get_object(JsonParser*, char*);

/**
 * Supprime un JsonParser
 * @param JsonParser* Le JsonParser à supprimer
 */
void clean_json_parser(JsonParser*);

//Json Encoder

/**
 * Initialise le JsonEncoder
 * @param JsonEncoder* Le JsonEncoder à initialiser
 */
void ini_encoder(JsonEncoder*);

/**
 * Ajoute une valeur au JSON
 * @param JsonEncoder* La structure pour encoder
 * @param char* La chaine à ajouter (sous la forme "key": val)
 */
void add_value(JsonEncoder*, char*);

/**
 * Ajoute un string au JSON
 * @param JsonEncoder* La structure pour encoder
 * @param char* La clef pour acceder à la valeur
 * @param char* La valeur
 */
void add_string(JsonEncoder*, char*, char*);

/**
 * Ajoute un nombre au JSON
 * @param JsonEncoder* La structure pour encoder
 * @param char* La clef pour acceder à la valeur
 * @param double La valeur
 * @param int Le nombre de chiffre après la virgule
 */
void add_number(JsonEncoder*, char*, double, int);

/**
 * Ajoute un entier au JSON
 * @param JsonEncoder* La structure pour encoder
 * @param char* La clef pour acceder à la valeur
 * @param int La valeur
 */
void add_integer(JsonEncoder*, char*, int);

/**
 * Ajoute un boolean au JSON
 * @param JsonEncoder* La structure pour encoder
 * @param char* La clef pour acceder à la valeur
 * @param boolean La valeur
 */
void add_boolean(JsonEncoder*, char*, boolean);

/**
 * Ajoute un tableau au JSON
 * @param JsonEncoder* La structure pour encoder
 * @param char* La clef pour acceder à la valeur
 * @param JsonArray* La valeur
 */
void add_array(JsonEncoder*, char*, JsonArray*);

/**
 * Ajoute un objet JSON au JSON
 * @param JsonEncoder* La structure pour encoder
 * @param char* La clef pour acceder à la valeur
 * @param JsonEncoder* La valeur
 */
void add_object(JsonEncoder*, char*, JsonEncoder*);

/**
 * Transforma en chaine de caractère au format JSON
 * @param JsonEncoder* La structure pour encoder
 * @return char* La chaine au format JSON
 */
char* json_encode(JsonEncoder*);

/**
 * Supprime les données du JsonEncoder
 * Après cette méthode la structure peut directement être réutilisée
 * @param JsonEncoder* La structure à supprimer
 */
void clean_json_encoder(JsonEncoder*);

//JsonArrayParser

/**
 * Initialise le JsonArray en mode Parser
 * @param JsonArray* La structure à initialiser
 */
void ini_array_parser(JsonArray*);

/**
 * Parse un tableau JSON
 * @param JsonArray* La structure initialisée en mode parser
 * @param char* La chaine JSON
 * @return int Le nombre d'élement dans le tableau ou JSON_ERROR
 */
int json_parse_array(JsonArray*, char*);

/**
 * Donne le nombre d'élément dans le tableau
 * @param JsonArray* Le resultat de json_parse_array
 * @return int Le nombre d'élémént
 */
int get_array_length(JsonArray*);
/**
 * Retourne le type de la valeur
 * @param JsonArray* Le resultat de json_parse_array
 * @param int La clef lié à la valeur
 * @return int constante JSON_XXXX du type de la valeur
 */
int get_array_type(JsonArray*, int);

/**
 * Retourne la valeur à l'index donnée du tableau
 * @param JsonArray* Le resultat de json_parse_array
 * @param int L'index
 * @return char* La valeur
 */
char* get_array_index(JsonArray*, int);

/**
 * Retourne la valeur d'un element
 * @param JsonArray* Le resultat de json_parse_array
 * @param int L'index
 * @return char* La valeur, NULL en cas d'erreur (index introuvable)
 */
char* get_array_value(JsonArray*, int);

/**
 * Retourne la valeur d'un element de type string
 * @param JsonArray* Le resultat de json_parse_array
 * @param int L'index
 * @return char* La valeur, NULL en cas d'erreur (type incorrect, index introuvable)
 */
char* get_array_string(JsonArray*, int);

/**
 * Retourne la valeur d'un element de type number
 * @param JsonArray* Le resultat de json_parse_array
 * @param int L'index
 * @return double La valeur, JSON_ERROR en cas d'erreur (type incorrect, index introuvable)
 */
double get_array_number(JsonArray*, int);

/**
 * Retourne la valeur d'un element de type number
 * @param JsonArray* Le resultat de json_parse_array
 * @param int L'index
 * @return int La valeur, JSON_ERROR en cas d'erreur (type incorrect, index introuvable)
 */
int get_array_integer(JsonArray*, int);

/**
 * Retourne la valeur d'un element de type boolean
 * @param JsonArray* Le resultat de json_parse_array
 * @param int L'index
 * @return boolean La valeur, false en cas d'erreur (type incorrect, index introuvable)
 */
boolean get_array_boolean(JsonArray*, int);

/**
 * Retourne la valeur d'un element de type array
 * @param @param JsonArray* Le resultat de json_parse_array
 * @param int L'index
 * @return JsonArray* La valeur, NULL en cas d'erreur (type incorrect, index introuvable)
 */
JsonArray* get_array_array(JsonArray*, int);

/**
 * Retourne la valeur d'un element de type object
 * @param JsonArray* Le resultat de json_parse_array
 * @param int L'index
 * @return JsonParser* L'object json analysé, NULL en cas d'erreur (type incorrect, index introuvable, ...)
 */
JsonParser* get_array_object(JsonArray*, int);

//JsonArrayEncoder

/**
 * Initialise le JsonArray en mode encoder
 * @param JsonArray* La structure à initialiser
 */
void ini_array_encoder(JsonArray*);

/**
 * Ajoute une valeur au JSON
 * @param JsonArray* La structure pour encoder
 * @param char* La chaine à ajouter
 */
boolean add_array_value(JsonArray*, char*);

/**
 * Ajoute un string au JSON
 * @param JsonArray* La structure pour encoder
 * @param char* La valeur
 */
boolean add_array_string(JsonArray*, char*);

/**
 * Ajoute un nombre au JSON
 * @param JsonArray* La structure pour encoder
 * @param char* La clef pour acceder à la valeur
 * @param double La valeur
 * @param int Le nombre de chiffre après la virgule
 */
boolean add_array_number(JsonArray*, double, int);

/**
 * Ajoute un entier au JSON
 * @param JsonArray* La structure pour encoder
 * @param int La valeur
 */
boolean add_array_integer(JsonArray*, int);

/**
 * Ajoute un boolean au JSON
 * @param JsonArray* La structure pour encoder
 * @param boolean La valeur
 */
boolean add_array_boolean(JsonArray*, boolean);

/**
 * Ajoute un tableau au JSON
 * @param JsonArray* La structure pour encoder
 * @param JsonArray* La valeur
 */
boolean add_array_array(JsonArray*, JsonArray*);

/**
 * Ajoute un objet JSON au JSON
 * @param JsonArray* La structure pour encoder
 * @param JsonEncoder* La valeur
 */
boolean add_array_object(JsonArray*, JsonEncoder*);

/**
 * Encode un tableau JSON
 * @param JsonArray* La structure à encoder
 * @return LA chaine JSON
 */
char* json_encode_array(JsonArray*);

//JsonArray

/**
 * Vide une structure JsonArray
 * @param JsonArray* La structure à vider
 */
void clean_json_array(JsonArray*);

#endif /* JSON_H */

