/* 
 * File:   str.h
 * Author: Arthur Brandao
 *
 * Created on 12 octobre 2018
 */
#ifndef STR_H
#define STR_H

#include <string.h>

/**
 * Creation d'un nouveau string vide
 * @param int Taille
 * @return char* Le string
 */
char* new_string(int);

/**
 * Copie un string
 * @param char* Le string à copier
 * @return char* Le string copié
 */
char* string_copy(char*);

/**
 * Retire les espaces avant et après la chaine
 * @param char* La chaine à modifier
 * @return La chaine modifiée
 */
char* trim(char*);

/**
 * Retire le caractère passé en paramètre avant et après la chaine
 * @param char* La chaine à modifier
 * @param char Le caractère à retirer
 * @return La chaine modifiée
 */
char* mtrim(char*, char);

/**
 * Retire le caractère passé en paramètre avant la chaine
 * @param char* La chaine à modifier
 * @param char Le caractère à retirer
 * @return La chaine modifiée
 */
char* ltrim(char*, char);

/**
 * Retire le caractère passé en paramètre après la chaine
 * @param char* La chaine à modifier
 * @param char Le caractère à retirer
 * @return La chaine modifiée
 */
char* rtrim(char*, char);

/**
 * Inverse la chaine de caractère
 * @param char* La chaine
 * @param int La taille de la chaine
 */
void reverse(char*, int);

/**
 * Convertit un float en chaine de caractère
 * @param float Le float à convertir
 * @param char* Le buffer de reception de la chaine
 * @param int Le nombre de chiffre après la virgule 
 */
void ftoa(float, char*, int);

/**
 * Supprime un craractere d'une chaine
 * @param char* La chaine à traiter
 * @param char Le caractere à supprimer
 * @return char* La chaine sans le caractere
 */
char* remove_char(char*, char);

#endif /* STR_H */

