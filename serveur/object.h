/* 
 * File:   object.h
 * Author: Arthur Brandao
 *
 * Created on 7 décembre 2018
 */

#ifndef OBJECT_H
#define OBJECT_H

/* --- Include --- */
#include "constante.h"

/* --- Constante --- */
#define OBJ_BCLASSIC 0
#define OBJ_BMINE 1
#define OBJ_BREMOTE 2
#define OBJ_BOMBUP 3
#define OBJ_BOMBDOWN 4
#define OBJ_FIREPOWER 5
#define OBJ_SCOOTER 6
#define OBJ_BROKENLEG 7
#define OBJ_LIFEUP 8
#define OBJ_LIFEMAX 9
#define OBJ_MAJOR 10

/* --- Structure --- */
typedef struct obj_node obj_node;
struct obj_node{
    int type;
    int x;
    int y;
    obj_node* prev;
    obj_node* next;
};
typedef struct{
    obj_node* first;
    obj_node* last;
    int size;
}Object;

/* --- Fonctions --- */
/**
 * Initialisation de la structure
 * @param Object* La structure à initialiser
 */
void object_ini(Object*);

/**
 * Ajoute un noeud
 * @param Object* L'objet à modifier
 * @param int Le type de l'objet
 * @param int La position X de l'objet
 * @param int La position Y de l'objet
 * @return Le noeud
 */
obj_node* object_add(Object*, int, int, int);

/**
 * Recherche un noeud avec le type et la position
 * @param Object* L'objet à modifier
 * @param int Le type de l'objet
 * @param int La position X de l'objet
 * @param int La position Y de l'objet
 * @return Le noeud 
 */
obj_node* object_search(Object*, int, int, int);

/**
 * Supprime un objet
 * @param Object* L'objet à modifier
 * @param obj_node* Le noeud à supprimer
 */
void object_delete(Object*, obj_node*);

/**
 * Vide la structure
 * @param Object* L'objet à vider
 */
void object_clean(Object*);

#endif /* OBJECT_H */

