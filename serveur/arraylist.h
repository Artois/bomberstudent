/* 
 * File:   arraylist.h
 * Author: Arthur Brandao
 *
 * Created on 14 novembre 2018
 */

#ifndef ARRAYLIST_H
#define ARRAYLIST_H

/* --- Include --- */
#include "constante.h"
#include "server.h"
#include "json.h"

/* --- Structure --- */
typedef struct al_node al_node;
struct al_node{
    char* key; //Clef d'accès
    int(*handler)(int, JsonParser*); //Fonction
    al_node* prev; //Noeud precedent
    al_node* next; //Noeud suivant
};
typedef struct{
    al_node* first; //1er noeud
    al_node* last; //Dernier noeud
    int size; //Nombre de noeud
}arraylist;

/* --- Fonctions --- */
/**
 * Initialise une arraylist
 * @param arraylist* Arraylist allouée
 */
void arraylist_ini(arraylist*);

/**
 * Ajoute un noeud dans la liste
 * Si un noeud du meme nom existe deja le remplace
 * @param arraylist* Arraylist d'accueil
 * @param char* clef
 * @param int(*)(int, JsonParser*) Fonction
 * @return al_node* Le noeud ajouté
 */
al_node* arraylist_add(arraylist*, char*, int(*)(int, JsonParser*));

/**
 * Cherche un noeud
 * @param arraylist* Arraylist utilisée pour la recherche
 * @param char* La clef à chercher
 * @return NULL|al_node* Le noeud ou null si introuvable
 */
al_node* arraylist_search(arraylist*, char*);

/**
 * Appel la fonction d'un noeud
 * @param arraylist* Arraylist du noeud
 * @param char* Clef du noeud
 * @param int Id du client
 * @param JsonParser* Argument pour la fonction
 * @return int Resultat de la fonction ou -1 si introuvable
 */
int arraylist_call(arraylist*, char*, int, JsonParser*);

/**
 * Supprime un noeud
 * @param arraylist* Arraylist du noeud
 * @param al_node* Le noeud à supprimer
 */
void arraylist_delete(arraylist*, al_node*);

/**
 * Supprime un noeud identifié par une clef
 * @param arraylist* Arraylist du noeud
 * @param char* La clef du noeud
 * @return Noeud correctement supprimé
 */
boolean arraylist_remove(arraylist*, char*);

/**
 * Vide une Arraylist
 * @param arraylist* Arraylist à vider
 */
void arraylist_clean(arraylist*);

#endif /* ARRAYLIST_H */

