/* 
 * File:   server.h
 * Author: Arthur Brandao
 *
 * Created on 14 novembre 2018
 */

#ifndef SERVER_H 
#define SERVER_H

/* --- Include --- */
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include "constante.h"

/* --- Constantes --- */
#define SERV_UDP 1
#define SERV_TCP 2

/* --- Structures --- */
typedef struct server* Server;
struct server{
	int type; /* Le type de serveur (UDP ou TCP) */
	/* Var server */
	int socket; /* Socket du serveur */
	int socket_client; /* Socket du client (TCP uniquement) */
	struct sockaddr_in serv; /* Adresse du serveur */
	struct sockaddr_in client; /* Adresse du client */
	socklen_t addr;
	/* Pointeur de fonction */
	boolean (*server_bind)(Server, int);
	ssize_t (*server_receive)(Server, char*, size_t);
	boolean (*server_send)(Server, char*);
	boolean (*server_accept)(Server);
};

/* --- Fonction --- */
/**
 * Creation d'un serveur UDP
 * @return Le serveur
 */
Server server_create_udp();

/**
 * Creation d'un serveur TCP
 * @return Le serveur
 */
Server server_create_tcp();

/**
 * Ferme un serveur
 * @param Server Le serveur à fermer
 */
void server_close(Server);

/**
 * Ferme la connexion avec un client d'une socket TCP
 * @param Server Ferme le client d'un serveur TCP
 * @return boolean Reussite
 */
boolean server_close_client(Server);

/**
 * Clone un serveur
 * @param Server Le serveur à cloner
 * @return Server Le serveur clonner
 */
Server server_clone(Server);

#endif /* SERVER_H */