/* 
 * File:   main.h
 * Author: Arthur Brandao
 *
 * Created on 22 novembre 2018
 */

#ifndef MAIN_H
#define MAIN_H

/* --- Include --- */
#include "constante.h"

/* --- Constantes --- */
#define END_USER 0
#define END_ERR 1

/* --- Fonctions --- */
/**
 * Notifie tous les clients connecté que le serveur s'arrete
 * @param int Status de fin du serveur
 */
void notify_close(int);


#endif /* MAIN_H */

