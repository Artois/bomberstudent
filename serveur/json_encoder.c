/* 
 * File:   json_parser.c
 * Author: Arthur Brandao
 *
 * Created on 29 octobre 2018
 */
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "json.h"

/* --- Fonctions publique --- */

void ini_encoder(JsonEncoder* this){
    this->max = JSON_MAX_SIZE;
    this->pos = 1;
    memset(this->json, 0, this->max);
    this->json[0] = '{';
}

void add_value(JsonEncoder* this, char* str){
    //Verif que l'on ne depasse pas (on retire 1 pour garder un \0 et on retire 1 autre pour pouvoir mettre })
    if(this->pos + strlen(str) >= this->max - 2){
        return;
    }
    //Ajoute
    for(int i = 0; i < strlen(str); i++){
        this->json[this->pos++] = str[i];
    }
}

void add_string(JsonEncoder* this, char* key, char* val){
    int length = strlen(key) + strlen(val) + 4 + 2 + 2; //clef + val + 4 guillemet + ": " + ", "
    //Verif que l'on ne depasse pas
    if(this->pos + length >= this->max - 2){
        return;
    }
    //Ajoute
    if(this->pos != 1){
        //Si pas 1er valeur on ajoute un separateur
        this->json[this->pos++] = ',';
        this->json[this->pos++] = ' ';
    }
    this->json[this->pos++] = '"';
    for(int i = 0; i < strlen(key); i++){
        this->json[this->pos++] = key[i];
    }
    this->json[this->pos++] = '"';
    this->json[this->pos++] = ':';
    this->json[this->pos++] = ' ';
    this->json[this->pos++] = '"';
    for(int i = 0; i < strlen(val); i++){
        this->json[this->pos++] = val[i];
    }
    this->json[this->pos++] = '"';
}

void add_number(JsonEncoder* this, char* key, double val, int ndigit){
    //Double en string
    char nombre[20];
    memset(nombre, 0, 20);
    ftoa(val, nombre, ndigit);
    //Creation chaine
    int length = strlen(key) + strlen(nombre) + 2 + 2 + 2; //clef + val + 2 guillemet + ": " + ", "
    //Verif que l'on ne depasse pas
    if(this->pos + length >= this->max - 2){
        return;
    }
    //Ajoute
    if(this->pos != 1){
        //Si pas 1er valeur on ajoute un separateur
        this->json[this->pos++] = ',';
        this->json[this->pos++] = ' ';
    }
    this->json[this->pos++] = '"';
    for(int i = 0; i < strlen(key); i++){
        this->json[this->pos++] = key[i];
    }
    this->json[this->pos++] = '"';
    this->json[this->pos++] = ':';
    this->json[this->pos++] = ' ';
    for(int i = 0; i < strlen(nombre); i++){
        this->json[this->pos++] = nombre[i];
    }
}

void add_integer(JsonEncoder* this, char* key, int val){
    //Double en string
    char nombre[20];
    memset(nombre, 0, 20);
    snprintf(nombre, 20, "%d", val);
    //Creation chaine
    int length = strlen(key) + strlen(nombre) + 2 + 2 + 2; //clef + val + 2 guillemet + ": " + ", "
    //Verif que l'on ne depasse pas
    if(this->pos + length >= this->max - 2){
        return;
    }
    //Ajoute
    if(this->pos != 1){
        //Si pas 1er valeur on ajoute un separateur
        this->json[this->pos++] = ',';
        this->json[this->pos++] = ' ';
    }
    this->json[this->pos++] = '"';
    for(int i = 0; i < strlen(key); i++){
        this->json[this->pos++] = key[i];
    }
    this->json[this->pos++] = '"';
    this->json[this->pos++] = ':';
    this->json[this->pos++] = ' ';
    for(int i = 0; i < strlen(nombre); i++){
        this->json[this->pos++] = nombre[i];
    }
}

void add_boolean(JsonEncoder* this, char* key, boolean val){
    //On determine le boolean
    char bool[6];
    memset(bool, 0, 6);
    if (val) {
        strncpy(bool, "true", 4);
    } else {
        strncpy(bool, "false", 5);
    }
    //Creation chaine
    int length = strlen(key) + strlen(bool) + 2 + 2 + 2; //clef + val + 2 guillemet + ": " + ", "
    //Verif que l'on ne depasse pas
    if(this->pos + length >= this->max - 2){
        return;
    }
    //Ajoute
    if(this->pos != 1){
        //Si pas 1er valeur on ajoute un separateur
        this->json[this->pos++] = ',';
        this->json[this->pos++] = ' ';
    }
    this->json[this->pos++] = '"';
    for(int i = 0; i < strlen(key); i++){
        this->json[this->pos++] = key[i];
    }
    this->json[this->pos++] = '"';
    this->json[this->pos++] = ':';
    this->json[this->pos++] = ' ';
    for(int i = 0; i < strlen(bool); i++){
        this->json[this->pos++] = bool[i];
    }
}

void add_array(JsonEncoder* this, char* key, JsonArray* val){
    //Verification
    if(val->mode != JSON_ARRAY_ENCODER){
        return;
    }
    //Recup string du JsonEncoder
    char* json;
    json = json_encode_array(val);
    //Creation chaine
    int length = strlen(key) + strlen(json) + 2 + 2 + 2; //clef + val + 2 guillemet + ": " + ", "
    //Verif que l'on ne depasse pas
    if(this->pos + length >= this->max - 2){
        free(json);
        return;
    }
    //Ajoute
    if(this->pos != 1){
        //Si pas 1er valeur on ajoute un separateur
        this->json[this->pos++] = ',';
        this->json[this->pos++] = ' ';
    }
    this->json[this->pos++] = '"';
    for(int i = 0; i < strlen(key); i++){
        this->json[this->pos++] = key[i];
    }
    this->json[this->pos++] = '"';
    this->json[this->pos++] = ':';
    this->json[this->pos++] = ' ';
    for(int i = 0; i < strlen(json); i++){
        this->json[this->pos++] = json[i];
    }
    //Nettoyage
    free(json);
}

void add_object(JsonEncoder* this, char* key, JsonEncoder* val){
    //Recup string du JsonEncoder
    char* json;
    json = json_encode(val);
    //Creation chaine
    int length = strlen(key) + strlen(json) + 2 + 2 + 2; //clef + val + 2 guillemet + ": " + ", "
    //Verif que l'on ne depasse pas
    if(this->pos + length >= this->max - 2){
        free(json);
        return;
    }
    //Ajoute
    if(this->pos != 1){
        //Si pas 1er valeur on ajoute un separateur
        this->json[this->pos++] = ',';
        this->json[this->pos++] = ' ';
    }
    this->json[this->pos++] = '"';
    for(int i = 0; i < strlen(key); i++){
        this->json[this->pos++] = key[i];
    }
    this->json[this->pos++] = '"';
    this->json[this->pos++] = ':';
    this->json[this->pos++] = ' ';
    for(int i = 0; i < strlen(json); i++){
        this->json[this->pos++] = json[i];
    }
    //Nettoyage
    free(json);
}

char* json_encode(JsonEncoder* this){
    char* json;
    //Ajoute } fin
    this->json[this->pos] = '}';
    //Creation chaine
    json = malloc(sizeof(char) * (this->pos + 2));
    memset(json, 0, this->pos + 2);
    strncpy(json, this->json, this->pos + 1);
    //Retourne
    return json;
}

void clean_json_encoder(JsonEncoder* this){
    ini_encoder(this);
}