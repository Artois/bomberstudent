/* 
 * File:   client.c
 * Author: Arthur Brandao
 *
 * Created on 21 novembre 2018
 */

#include <stdio.h>
#include <stdlib.h>
#include "client.h"

/* --- Globale --- */
int id = 0;
ClientList clist;

/* --- Fonctions publiques ---*/
int add_client(Server cli, Server serv){
    //Creation client
    Client* c = malloc(sizeof(Client));
    c->main = server_clone(cli);
    c->notify = server_clone(serv);
    c->id = id++;
    //Creation noeud
    ClientNode* cn = malloc(sizeof(ClientNode));
    cn->cli = c;
    cn->next = NULL;
    cn->empty = false;
    //Ajout dans la liste
    if(clist.length == 0){
        clist.tab = cn;
    } else {
        ClientNode* lastcn = clist.tab;
        for(int i = 0; i < clist.length - 1; i++){
            lastcn = lastcn->next;
        }
        lastcn->next = cn;
    }
    clist.length++;
    //Retourne l'id du client
    return c->id;
}

Client* get_client(int id){
    //Si hors du tableau
    if(id >= clist.length){
        return NULL;
    }
    //Recup le noeud
    ClientNode* cn = clist.tab;
    for(int i = 0; i < id; i++){
        cn = cn->next;
    }
    //Si client suppr
    if(cn->empty){
        return NULL;
    }
    return cn->cli;
}

void remove_client(int id){
    //Si hors du tableau
    if(id >= clist.length){
        return;
    }
    //Recup le noeud
    ClientNode* cn = clist.tab;
    for(int i = 0; i < id; i++){
        cn = cn->next;
    }
    //Supprime le client
    server_close_client(cn->cli->main);
    server_close_client(cn->cli->notify);
    free(cn->cli->main);
    free(cn->cli->notify);
    free(cn->cli);
    cn->empty = true;
}

void clean_clientlist(){
    ClientNode* tmp;
    ClientNode* cn = clist.tab;
    for(int i = 0; i < clist.length; i++){
        if(!cn->empty){
            remove_client(i);
        }
        tmp = cn;
        cn = cn->next;
        free(tmp);
    }
    clist.length = 0;
    clist.tab = NULL;
}

int get_number_client(){
    return clist.length;
}