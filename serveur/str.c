/* 
 * File:   str.c
 * Author: Arthur Brandao
 *
 * Created on 12 octobre 2018
 */
#define _POSIX_C_SOURCE 200112L

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "str.h"

/* --- Fonctions privées --- */

// Converts a given integer x to string str[].  d is the number 
// of digits required in output. If d is more than the number 
// of digits in x, then 0s are added at the beginning. 
int intToStr(int x, char str[], int d){ 
    int i = 0; 
    while (x) 
    { 
        str[i++] = (x%10) + '0'; 
        x = x/10; 
    } 
  
    // If number of digits required is more, then 
    // add 0s at the beginning 
    while (i < d) 
        str[i++] = '0'; 
  
    reverse(str, i); 
    str[i] = '\0'; 
    return i; 
} 

/* --- Fonctions publiques --- */

char* new_string(int length){
    char* str = malloc(sizeof(char) * (length + 1));
    memset(str, 0, length + 1);
    return str;
}

char* string_copy(char* src){
    int length = strlen(src);
    char* dest = new_string(length);
    strncpy(dest, src, length);
    return dest;
}

char* trim(char* str){
    return ltrim(rtrim(str, ' '), ' ');
}

char* mtrim(char* str, char mask){
    return ltrim(rtrim(str, mask), mask);
}

char* ltrim(char* str, char mask){
    //Variable
    int cmpt = 0;
    char* res;
    //Compte le nombre d'espace
    while(str[cmpt] == mask){
        cmpt++;
    }
    //Si aucun espace au debut
    if(cmpt == 0){
        res = malloc(sizeof(char) * (strlen(str) + 1));
        memset(res, 0, strlen(str) + 1);
        strcpy(res, str);
        return res;
    }
    //Sinon creation nouvelle chaine
    res = malloc(sizeof(char) * (strlen(str) - cmpt + 1));
    memset(res, 0, strlen(str) - cmpt + 1);
    for(int i = 0, j = cmpt; j < (strlen(str)); i++, j++){
        res[i] = str[j];
    }
    //Retour nouvelle chaine
    return res;
}

char* rtrim(char* str, char mask){
    //Variable
    int cmpt = strlen(str) - 1;
    char* res;
    //Compte le nombre d'espace
    while(str[cmpt] == mask){
        cmpt--;
    }
    //Si aucun espace au debut
    if(cmpt == strlen(str) - 1){
        res = malloc(sizeof(char) * (strlen(str) + 1));
        memset(res, 0, strlen(str) + 1);
        strcpy(res, str);
        return res;
    }
    cmpt++;
    //Sinon creation nouvelle chaine
    res = malloc(sizeof(char) * (cmpt + 2));
    memset(res, 0, cmpt + 2);
    for(int i = 0; i < cmpt; i++){
        res[i] = str[i];
    }
    //Retour nouvelle chaine
    return res;
}

void reverse(char *str, int len){ 
    int i=0, j=len-1, temp; 
    while (i<j) 
    { 
        temp = str[i]; 
        str[i] = str[j]; 
        str[j] = temp; 
        i++; j--; 
    } 
} 

void ftoa(float n, char *res, int afterpoint) { 
    // Extract integer part 
    int ipart = (int)n; 
  
    // Extract floating part 
    float fpart = n - (float)ipart; 
  
    // convert integer part to string 
    int i = intToStr(ipart, res, 0); 
  
    // check for display option after point 
    if (afterpoint != 0) 
    { 
        res[i] = '.';  // add dot 
  
        // Get the value of fraction part upto given no. 
        // of points after dot. The third parameter is needed 
        // to handle cases like 233.007 
        fpart = fpart * pow(10, afterpoint); 
  
        intToStr((int)fpart, res + i + 1, afterpoint); 
    } 
} 

char* remove_char(char* src, char carac){
    int length, compteur = 0;
    char* tmp = src;
    char* str;
    //Compte le nombre de fois ou le caracère apparait
    while(*tmp){
        if(*tmp == carac){
            compteur++;
        }
        tmp++;
    }
    //Creation nouvelle chaine
    length = strlen(src) - compteur;
    str = new_string(length);
    tmp = str;
    //Copie la chaine
    while(*src){
        if(*src != carac){
            *tmp = *src;
            tmp++;
        }
        src++;
    }
    return str;
}