/* 
 * File:   player.c
 * Author: Arthur Brandao
 *
 * Created on 28 novembre 2018
 */

#include <stdio.h>
#include <stdlib.h>
#include "player.h"
#include "object.h"

/* --- Fonctions publiques --- */

void create_player(Player* p, Client* c){
    //Initialisation valeurs
    p->ini = true;
    p->id = c->id;
    p->cli = c;
    p->x = 1;
    p->y = 1;
    p->life = 100;
    p->maxLife = 100;
    p->speed = 1;
    p->classicBomb = 1;
    p->mine = 1;
    p->remoteBomb = 1;
    p->maxBomb = 2;
    p->nbBomb = 0;
    p->bombUp = 0;
    p->bombDown = 0;
    p->firePower = 0;
    p->scooter = 0;
    p->brokenLeg = 0;
    p->lifeMax = 0;
    p->lifeUp = 0;
    p->major = 0;
    p->bomb = malloc(sizeof(Object));
    object_ini(p->bomb);
}

void describe_player(Player* p, JsonEncoder* desc){
    JsonEncoder* bonus = malloc(sizeof(JsonEncoder));
    JsonArray* bm = malloc(sizeof(JsonArray));
    //Initialisation
    ini_encoder(desc);
    ini_encoder(bonus);
    ini_array_encoder(bm);
    //Calcul bonus malus
    if(p->bombUp > 0){
        add_string(bonus, "class", "bomb_up");
        add_integer(bonus, "number", p->bombUp);
        add_array_object(bm, bonus);
        clean_json_encoder(bonus);
    }
    if(p->bombDown > 0){
        add_string(bonus, "class", "bomb_down");
        add_integer(bonus, "number", p->bombDown);
        add_array_object(bm, bonus);
    }
    if(p->firePower > 0){
        add_string(bonus, "class", "fire_power");
        add_integer(bonus, "number", p->firePower);
        add_array_object(bm, bonus);
        clean_json_encoder(bonus);
    }
    if(p->scooter > 0){
        add_string(bonus, "class", "scooter");
        add_integer(bonus, "number", p->scooter);
        add_array_object(bm, bonus);
        clean_json_encoder(bonus);
    }
    if(p->brokenLeg > 0){
        add_string(bonus, "class", "broken_leg");
        add_integer(bonus, "number", p->brokenLeg);
        add_array_object(bm, bonus);
        clean_json_encoder(bonus);
    }
    if(p->lifeUp > 0){
        add_string(bonus, "class", "life_up");
        add_integer(bonus, "number", p->lifeUp);
        add_array_object(bm, bonus);
        clean_json_encoder(bonus);
    }
    if(p->lifeMax > 0){
        add_string(bonus, "class", "life_max");
        add_integer(bonus, "number", p->lifeMax);
        add_array_object(bm, bonus);
        clean_json_encoder(bonus);
    }
    if(p->major > 0){
        add_string(bonus, "class", "major");
        add_integer(bonus, "number", p->major);
        add_array_object(bm, bonus);
        clean_json_encoder(bonus);
    }
    //Ajout valeur
    add_integer(desc, "id", p->id);
    add_integer(desc, "life", p->life);
    add_integer(desc, "maxLife", p->maxLife);
    add_integer(desc, "speed", p->speed);
    add_integer(desc, "currentNbClassicBomb", p->classicBomb);
    add_integer(desc, "currentNbMine", p->mine);
    add_integer(desc, "currentNbRemoteBomb", p->remoteBomb);
    add_integer(desc, "maxNbBomb", p->maxBomb);
    add_array(desc, "bonus-malus", bm);
    //Nettoyage
    clean_json_array(bm);
    free(bonus);
    free(bm);
}

void describe_short_player(Player* p, JsonEncoder* desc){
    char pos[40];
    //Creation chaine
    memset(pos, 0, 40);
    snprintf(pos, 40, "%d,%d", p->x, p->y);
    //Creation JSON
    ini_encoder(desc);
    add_integer(desc, "id", p->id);
    add_string(desc, "pos", pos);
}

void add_player_object(Player* p, int type){
    switch(type){
        case OBJ_BCLASSIC:
            p->classicBomb++;
            break;
        case OBJ_BMINE:
            p->mine++;
            break;
        case OBJ_BREMOTE:
            p->remoteBomb++;
            break;
        case OBJ_BOMBUP:
            p->bombUp++;
            p->maxBomb++;
            break;
        case OBJ_BOMBDOWN:
            p->bombDown++;
            if(p->maxBomb > 1){
                p->maxBomb--;
            }
            break;
        case OBJ_FIREPOWER:
            p->firePower++;
            break;
        case OBJ_SCOOTER:
            p->scooter++;
            p->speed++;
            break;
        case OBJ_BROKENLEG:
            p->brokenLeg++;
            if(p->speed > 1){
                p->speed--;
            }
            break;
        case OBJ_LIFEUP:
            p->lifeUp++;
            p->maxLife = (int) p->maxLife * 1.1;
            break;
        case OBJ_LIFEMAX:
            p->lifeMax++;
            p->life = p->maxLife;
            break;
        case OBJ_MAJOR:
            if(p->major == 0){
                p->major = 1;
            }
            break;
    }
}

void delete_player(Player* p){
    p->cli = NULL;
    p->ini = false;
    object_clean(p->bomb);
    free(p->bomb);
}