/* 
 * File:   delay.c
 * Author: Arthur Brandao
 *
 * Created on 4 décembre 2018
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>
#include "error.h"
#include "delay.h"
#include "bomberstudent_server.h"

/* --- Extern --- */
extern Game game[MAXGAME];
extern int nbGame;
extern pthread_mutex_t gameMutex[MAXGAME];
extern pthread_mutex_t playerMutex[MAXGAME * MAXPLAYER];

/* --- Fonctions privées --- */

/**
 * Thread d'attente de la fonction timer
 * @param void* La structure timer_data cast en void*
 * @return NULL
 */
void* delay_thread(void* data) {
    delay_t* d;
    //Recup données
    d = (delay_t*) data;
    //Detache le thread
    if (pthread_detach(pthread_self()) != 0) {
        adderror("Impossible de détacher le thread Delay");
        return NULL;
    }
    //Attend le temps indiqué
    sleep(d->second);
    //Appel callback
    pthread_mutex_lock(&gameMutex[d->game]);
    if (game[d->game].active) {
        pthread_mutex_lock(&playerMutex[(d->game * MAXPLAYER) + d->player]);
        if (game[d->game].player[d->player]->ini) {
            if (d->callback(&game[d->game], d->player, d->data) != SUCCESS) {
                adderror("Erreur callback Delay");
            }
        }
        pthread_mutex_unlock(&playerMutex[(d->game * MAXPLAYER) + d->player]);
    }
    pthread_mutex_unlock(&gameMutex[d->game]);
    //Free data
    if(d->data != NULL){
        free(data);
    }
    return NULL;
}

/* --- Fonctions publiques --- */
void delay(int second, int game, int player, int(*callback)(Game*, int, void*)) {
    pthread_t thread;
    delay_t* d = malloc(sizeof (delay_t));
    d->second = second;
    d->game = game;
    d->player = player;
    d->data = NULL;
    d->callback = callback;
    //Creation thread
    if (pthread_create(&thread, NULL, delay_thread, d) != 0) {
        adderror("Impossible de créer le thread Delay");
    }
}

void delay_data(int second, int game, int player, void* data, int(*callback)(Game*, int, void*)) {
    pthread_t thread;
    delay_t* d = malloc(sizeof (delay_t));
    d->second = second;
    d->game = game;
    d->player = player;
    d->data = data;
    d->callback = callback;
    //Creation thread
    if (pthread_create(&thread, NULL, delay_thread, d) != 0) {
        adderror("Impossible de créer le thread Delay");
    }
}

int callback_major_end(Game* g, int playerIndex, void* data) {
    JsonEncoder notif;
    //Retire le bonus major
    g->player[playerIndex]->major = 0;
    //Creation json
    ini_encoder(&notif);
    describe_player(g->player[playerIndex], &notif);
    //Envoi
    if (!notify_client(g->player[playerIndex]->cli, "POST", "player/major/end", &notif)) {
        return FAIL;
    }
    return SUCCESS;
}

int callback_bomb_explode(Game* g, int playerIndex, void* data){
    JsonEncoder notif;
    int* pos = (int*) data;
    ini_encoder(&notif);
    //Retire mutex car mis dans la fonction
    pthread_mutex_unlock(&playerMutex[(g->index * MAXPLAYER) + playerIndex]);
    pthread_mutex_unlock(&gameMutex[g->index]);
    //Gestion des explosions
    if(bomb_explode(g, playerIndex, pos[0], pos[1], &notif)){
        //Notification joueurs
        if(!notify_player(g, "POST", "attack/explose", &notif, -1)){
            clean_json_encoder(&notif);
            return FAIL;
        }
    }
    //Remise des mutex
    pthread_mutex_lock(&gameMutex[g->index]);
    pthread_mutex_lock(&playerMutex[(g->index * MAXPLAYER) + playerIndex]);
    clean_json_encoder(&notif);
    return SUCCESS;
}