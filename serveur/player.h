/* 
 * File:   player.h
 * Author: Arthur Brandao
 *
 * Created on 28 novembre 2018
 */

#ifndef PLAYER_H
#define PLAYER_H

/* --- Include --- */
#include "constante.h"
#include "client.h"
#include "json.h"
#include "object.h"

/* --- Structure --- */
typedef struct{
    boolean ini; //Indique si initalisé
    /* Stats basique */
    int id; //Id du joueur <=> id du Client
    Client* cli; //Client pour communiquer avec le joueur
    int x; //Position X (gauche droite)
    int y; //Position Y (haut bas)
    int life; //Vie
    int maxLife; //Vie max
    int speed; //Vitesse de deplacement
    int classicBomb; //Nombre de bombe classique
    int mine; //Nombre de mine
    int remoteBomb; //Nombre de bombe télécommandée
    int maxBomb; //Nombre max de bombe simultané sur le terrain
    int nbBomb; //Nombre de bombe actuellement sur le terrain
    /* Bonus (Nombre de fois ou le bonus est pris). Les modifications sont toujours calculées par le serveur */
    int bombUp;
    int bombDown;
    int firePower;
    int scooter;
    int brokenLeg;
    int lifeMax;
    int lifeUp;
    int major;
    /* Les bombes posées par le joueur */
    Object* bomb;
}Player;

/* --- Fonctions --- */
/**
 * Création d'un joueur
 * @param Player* La structure à initialiser
 * @param Client* La structure pour communiquer avec le joueur
 */
void create_player(Player*, Client*);

/**
 * Decris un joueur en JSON
 * @param Player* Le joueur à décrire
 * @param JsonEncoder* Le json d'accueil
 */
void describe_player(Player*, JsonEncoder*);

/**
 * Decripstion courte d'un joueur en JSON
 * @param Player* Le joueur à décrire
 * @param JsonEncoder* Le json d'accueil
 */
void describe_short_player(Player*, JsonEncoder*);

/**
 * Ajoute un objet à un joueur
 * @param Player* Le joueur
 * @param int Le type de l'objet
 */
void add_player_object(Player*, int);

/**
 * Supprime un joueur
 * @param Player* La structure à supprimer
 */
void delete_player(Player*);

#endif /* PLAYER_H */

