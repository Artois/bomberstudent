/* 
 * File:   object.c
 * Author: Arthur Brandao
 *
 * Created on 7 décembre 2018
 */

#include <stdio.h>
#include <stdlib.h>
#include "object.h"

/* --- Fonctions publiques --- */
void object_ini(Object* this){
    this->first = NULL;
    this->last = NULL;
    this->size = 0;
}

obj_node* object_add(Object* this, int type, int x, int y){
    obj_node* objn;
    //Ajout des vthiseurs
    objn = malloc(sizeof(obj_node));
    objn->type = type;
    objn->x = x;
    objn->y = y;
    //Lien entre les noeuds
    objn->next = NULL;
    if (this->first == NULL) {
        objn->prev = NULL;
        this->first = objn;
        this->last = objn;
    } else {
        objn->prev = this->last;
        this->last->next = objn;
        this->last = objn;
    }
    this->size++;
    return objn;
}

obj_node* object_search(Object* this, int type, int x, int y){
    //Si liste vide
    if(this->size == 0){
        return NULL;
    }
    //Recherche
    obj_node* objn = this->first;
    while(objn != NULL){
        if(objn->type == type && objn->x == x && objn->y == y){
            return objn;
        }
        objn = objn->next;
    }
    return NULL;
}

void object_delete(Object* this, obj_node* objn){
    //Liste vide
    if(this->first == NULL){
        return;
    }
    //Si 1er et seul
    if (objn->prev == NULL && objn->next == NULL) {
        this->first = NULL;
        this->last = NULL;
    }        
    //Si 1er et non seul
    else if (objn->prev == NULL && objn->next != NULL) {
        objn->next->prev = NULL;
        this->first = objn->next;
    }        
    //Si dernier
    else if (objn->next == NULL) {
        objn->prev->next = NULL;
        this->last = objn->prev;
    }        
    //Sinon si il est au milieu
    else {
        objn->prev->next = objn->next;
        objn->next->prev = objn->prev;
    }
    //Free
    free(objn);
    this->size--;
}

void object_clean(Object* this){
    //Liste vide
    if(this->first == NULL){
        return;
    }
    //Vide liste
    obj_node* tmp, * objn = this->first;
    while (objn != NULL) {
        tmp = objn->next;
        object_delete(this, objn);
        objn = tmp;
    }
    this->first = NULL;
    this->last = NULL;
    this->size = 0;
}