/* 
 * File:   handler.h
 * Author: Arthur Brandao
 *
 * Created on 23 novembre 2018
 */

#ifndef HANDLER_H
#define HANDLER_H

/* --- Include --- */
#include "constante.h"
#include "json.h"
#include "game.h"

/* --- Fonctions --- */
/**
 * Initialise les handlers
 */
void ini_handler();

/**
 * Handler de gestion des deconnexion des clients
 * @param int L'id du client
 * @param JsonParser* Le json envoyer par le client
 * @return int Etat
 */
int handler_client_end(int, JsonParser*);

/**
 * Liste des games actives
 * @param int L'id du client
 * @param JsonParser* Le json envoyer par le client
 * @return int Etat
 */
int handler_game_list(int, JsonParser*);

/**
 * Creation d'une partie
 * @param int L'id du client
 * @param JsonParser* Le json envoyer par le client
 * @return int Etat
 */
int handler_game_create(int, JsonParser*);

/**
 * Rejoindre une partie
 * @param int L'id du client
 * @param JsonParser* Le json envoyer par le client
 * @return int Etat
 */
int handler_game_join(int, JsonParser*);

/**
 * Quitter une partie
 * @param int L'id du client
 * @param JsonParser* Le json envoyer par le client
 * @return int Etat
 */
int handler_game_quit(int, JsonParser*);

/**
 * Mouvement d'un joueur
 * @param int L'id du client
 * @param JsonParser* Le json envoyer par le client
 * @return int Etat
 */
int handler_player_move(int, JsonParser*);

/**
 * Rammaser un objet
 * @param int L'id du client
 * @param JsonParser* Le json envoyer par le client
 * @return int Etat
 */
int handler_object_new(int, JsonParser*);

/**
 * Poser une bombe
 * @param int L'id du client
 * @param JsonParser* Le json envoyer par le client
 * @return int Etat
 */
int handler_attack_bomb(int, JsonParser*);

/**
 * Activer les remotes bombes
 * @param int L'id du client
 * @param JsonParser* Le json envoyer par le client
 * @return int Etat
 */
int handler_attack_remote_go(int, JsonParser*);

#endif /* HANDLER_H */

