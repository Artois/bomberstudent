/* 
 * File:   bomberstudent_server.h
 * Author: Arthur Brandao
 *
 * Created on 14 novembre 2018
 */

#ifndef BOMBERSTUDENT_SERVER_H
#define BOMBERSTUDENT_SERVER_H

/* --- Include --- */
#include "constante.h"
#include "server.h"
#include "json.h"
#include "client.h"

/* --- Constantes --- */
#define PORT_UDP 18624
#define PORT_TCP 18642
#define EUNKNOWN 0 //Erreur inconnue (520)
#define EREQUEST 1 //Erreur requete (400)
#define EFORBIDDEN 2 //Interdit (403)

/* --- Fonctions --- */
/**
 * Initialise les variables du serveur
 */
void ini_server();

/**
 * Ajoute un handler
 * @param char* La methode lié au handler
 * @param char* La ressource lié au handler
 * @param int(*)(int, JsonParser*) La fonction à appeler
 */
void add_handler(char*, char*, int(*)(int, JsonParser*));

/**
 * Lance le serveur UDP pour permettre d'être trouver dans le reseau
 * @param int Port UDP
 * @return Reussite
 */
boolean launch_udp_server(int);

/**
 * Lance les serveurs TCP sur port et port + 1, pour respectivement communiquer
 * et notifier le client
 * @param int Le port
 * @return Reussite
 */
boolean launch_tcp_server(int);

/**
 * Attente reception d'une requete du client
 * @param Client* Le client dont ont attend une requete
 * @return Reussite
 */
boolean receive_client(Client*);

/**
 * Repond au client
 * @param int L'id du client
 * @param JsonEncoder* Les données à envoyer
 * @return Reussite
 */
boolean send_client(int, JsonEncoder*);

/**
 * Envoi un message d'erreur au client
 * @param int L'id du client
 * @param int Le numero de l'erreur
 * @return Reussite
 */
boolean send_err_client(int, int);

/**
 * Notifie les clients
 * @param Client* Le client à notifier
 * @param char* Méthode à utiliser (POST ou GET)
 * @param char* La ressource demandé
 * @param JsonEncoder* Les parametres
 * @return Reussite
 */
boolean notify_client(Client*, char*, char*, JsonEncoder*);

/**
 * Notifie tous les clients connecté
 * @param char* Méthode à utiliser (POST ou GET)
 * @param char* La ressource demandé
 * @param JsonEncoder* Les parametres
 * @return Reussite
 */
boolean notify_all(char*, char*, JsonEncoder*);

#endif /* BOMBERSTUDENT_SERVER_H */

