/* 
 * File:   client.h
 * Author: Arthur Brandao
 *
 * Created on 21 novembre 2018
 */

#ifndef CLIENT_H
#define CLIENT_H

/* --- Include --- */
#include "constante.h"
#include "server.h"

/* --- Strcutures --- */
typedef struct{
    int id; //Id unique du client
    Server main; //Connexion principale entre le client et le serveur
    Server notify; //Connexion pour notifier le client
}Client;
typedef struct ClientNode ClientNode;
struct ClientNode{
    boolean empty;
    Client* cli;
    ClientNode* next;
};
typedef struct{
    int length;
    ClientNode* tab;
}ClientList;

/* --- Fonctions --- */
/**
 * Ajoute un client dans lalsite
 * @param Server Le serveur TCP à utiliser pour la connexion principale
 * @param Server Le serveur TCP à utiliser pour notifier le client
 * @return int L'id du client
 */
int add_client(Server, Server);

/**
 * Retourne un client
 * @param int L'id du client
 * @return Client* Le client
 */
Client* get_client(int);

/**
 * Supprime un client
 * @param int L'id du client
 */
void remove_client(int);

/**
 * Vide la liste des clients
 */
void clean_clientlist();

/**
 * Retourne le nombre de client actuel dans la liste
 * @return int Le nombre de client
 */
int get_number_client();

#endif /* CLIENT_H */

