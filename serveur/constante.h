/* 
 * File:   constante.h
 * Author: Arthur Brandao
 *
 * Created on 31 octobre 2018
 */

#ifndef CONSTANTE_H
#define CONSTANTE_H

/* --- General --- */
#define BUFFER_SIZE 2048

/* --- Fichier --- */
#define STDIN 0
#define STDOUT 1
#define STDERR 2

/* --- Boolean --- */
#define boolean int
#define true 1
#define false 0

/* --- Null --- */
#define null NULL

/* --- Reussite --- */
#define FAIL 0
#define SUCCESS 1

/* --- Application --- */
#define GET 1
#define POST 2
#define NBERRORRESET 3 //Nombre d'erreur avant de considérer que la connexion comme fermée 
#define MAXGAME 5 //Nombre maximum de parti autorisé en simultané
#define MAXPLAYER 4 //Nombre max de joueur par parti
#define MAPDIR "map/" //Chemin vers le dossier des maps
#define TIMEMAJOR 10 //Temps du major en seconde
#define TIMEBOMB 4 //Nombre de seconde avant explosion de la bombe
#define SPAWNRATE 30 //% de chance de spawn d'un objet
#define DAMAGEBOMB 0.4 //% de degat des bombes
#define DAMAGEMINE 0.6 //% de degat des mines

/* --- Reseaux --- */
#define PORT_UDP 18624
#define PORT_TCP 18642
#define SEARCH_SERV "looking for bomberstudent servers"

#endif /* CONSTANTE_H */

