/* 
 * File:   file.h
 * Author: Arthur Brandao
 *
 * Created on 23 novembre 2018
 */

#ifndef FILE_H
#define FILE_H

/* --- Include --- */
#include "constante.h"

/* --- Fonctions --- */
/**
 * Indique si un chemin correspond bien à un dossier
 * @param char* Le chemin
 * @return Si c'est un dossier ou non
 */
boolean is_dir(const char*);

/**
 * Liste les fichiers d'un dossier (ne prend pas en compte les fichiers cachées)
 * @param char* Le chemin vers le dossier
 * @param int* Le nombre de resultat trouver par la fonction
 * @return Le tableau de chaine de caractère si réussite, NULL si rien ou erreur
 */
char** file_list(const char*, int*);

/**
 * Liste les fichiers d'un dossier
 * @param char* Le chemin vers le dossier
 * @param int* Le nombre de resultat trouver par la fonction
 * @return Le tableau de chaine de caractère si réussite, NULL si rien ou erreur
 */
char** file_list_all(const char*, int*);

/**
 * Récupère le contenu d'un fichier
 * @param char* Chemin vers le fichier
 * @return Le contenu du fichier ou NULL en cas d'erreur
 */
char* file_get_content(const char*);

#endif /* FILE_H */

