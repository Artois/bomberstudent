/* 
 * File:   main.c
 * Author: Arthur Brandao
 *
 * Created on 22 novembre 2018
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include "error.h"
#include "bomberstudent_server.h"
#include "main.h"
#include "handler.h"
#include "game.h"

/* --- Extern --- */
extern Error error;

/* --- Global --- */
boolean stop = false;

/* --- Fonctions privées --- */
void handler(int sig){
    //Remet le handler
    signal(sig, handler);
    //Si SIGINT
    if(sig == SIGINT){
        printf("\nArret du serveur\n");
        stop = true;
        //Avertit les clients
        printf("Notifie les clients de l'arret du serveur\n");
        notify_close(END_USER);
        clean_clientlist();
        //Stop les parties
        clean_games();
    }
}

/* --- Fonctions publiques --- */
void notify_close(int code){
    char codestr[3];
    memset(codestr, 0, 3);
    sprintf(codestr, "%d", code);
    //Encode la reponse
    JsonEncoder je;
    ini_encoder(&je);
    add_string(&je, "status", codestr);
    add_string(&je, "message", "Server close");
    //Avertit tous les clients
    notify_all("POST", "server/end", &je);
}

int main(){
    //Lancement gestion d'erreur
    error_finit("bomberstudent_server.log");
    //Inialise le serveur
    ini_server();
    //Gestion des signaux
    signal(SIGINT, handler);
    signal(SIGPIPE, SIG_IGN); //Ignore les signaux SIGPIPE (on est pas sur d'en recevoir à chaque coupure socket + Impossible de determnier quelle socket est fermé (on laisse la méthode du timeout pour la trouver)
    //Initialisation des composants
    ini_handler();
    if(!ini_games()){
        error.print("Impossible d'initialiser le serveur");
        error.exit_err();
    }
    //Lance le serveur UDP
    printf("Lancement serveur UDP\n");
    if(!launch_udp_server(PORT_UDP)){
        error.print("Impossible de démarrer le serveur");
        error.exit_err();
    }
    //Lance les seerveurs TCP
    printf("Lancement serveurs TCP\n");
    if(!launch_tcp_server(PORT_TCP)){
        error.print("Impossible de démarrer le serveur");
        error.exit_err();
    }
    //Attend la fin du programme
    printf("Serveur Bomberstudent actif\n");
    while(!stop){
        pause();
    } 
    printf("Fin de l'execution\n");
    error.end();
    return EXIT_SUCCESS;
}