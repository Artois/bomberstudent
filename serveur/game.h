/* 
 * File:   game.h
 * Author: Arthur Brandao
 *
 * Created on 28 novembre 2018
 */

#ifndef GAME_H
#define GAME_H

/* --- Include --- */
#include <pthread.h>
#include "constante.h"
#include "player.h"
#include "json.h"
#include "object.h"

/* --- Constante --- */
#define WIDTH 0
#define HEIGHT 1

/* --- Struct --- */
typedef struct{
    boolean active;
    int index;
    char* name; //Nom
    int nbPlayer; //Nombre de joueur
    char* mapName; //Nom de la map
    char* mapContent; //La map en string
    int width; //Largeur de la map
    int height; //Hauteur de la map
    char** map; //Map
    Player* player[MAXPLAYER]; //Les joueurs actuels
    Object* object; //Les objets sur la map
}Game;

/* --- Extern --- */
extern Game game[MAXGAME];
extern int nbGame;
extern pthread_mutex_t gameMutex[MAXGAME];
extern pthread_mutex_t playerMutex[MAXGAME * MAXPLAYER];

/* --- Fonctions --- */
/**
 * Initialise les structures des parties
 */
boolean ini_games();

/**
 * Liste le nom de toutes les maps sous forme de JSON
 * @param JsonArray* Structure de reponse pour les maps existantes
 */
void list_map(JsonArray*);

/**
 * Liste les game en cours en JSON
 * @param JsonArray* Structure de reponse pour les games existantes
 * @return int Le nombre de game active
 */
int list_game(JsonArray*);

/**
 * Donne les dimension d'une carte
 * @param char* La carte
 * @return int* Tableau de resultat
 */
int* map_size(char*);

/**
 * Création d'une game
 * @param char* Nom de la game
 * @param char* Nom de la map
 * @return L'index de la game dans le tableau ou ERR
 */
int create_game(char*, char*);

/**
 * Ajoute un joueur
 * @param Game* La game dans laquelle ajouter le joueur
 * @param int Id du client lié au joueur
 * @return L'index du joueur dans la game ou ERR
 */
int add_player(Game*, int);

/**
 * Decris une game en JSON
 * @param Game* La game à décrire
 * @param int Index du joueur à décrire en plus de la game (-1 pour rien)
 * @param JsonEncoder* Le json pour mettre la description
 */
void describe_game(Game*, int, JsonEncoder*);

/**
 * Notification des joueurs d'une partie
 * @param Game* La partie des joueurs
 * @param char* Action (POST ou GET)
 * @param char* La ressource
 * @param JsonEncoder* Les paramètres JSON à envoyer
 * @param int L'id d'un joueur à ne pas notifier
 * @return 
 */
boolean notify_player(Game*, char*, char*, JsonEncoder*, int);

/**
 * Indique si il y a un collision avec un joueur
 * @param Game* La partie dans laquelle vérifier
 * @param int X
 * @param int Y
 * @return Collision ou non
 */
boolean player_collision(Game*, int, int);

/**
 * Indique si il y a un collision avec un joueur et retourne l'index
 * @param Game* La partie dans laquelle vérifier
 * @param int X
 * @param int Y
 * @param int* L'index du joueur avec laquelle la collision à lieu
 * @return Collision ou non
 */
boolean player_collision_index(Game*, int, int, int*);

/**
 * Explose un bombe
 * @param Game* La game de la bombe
 * @param int L'index du joueur à qui appartient la bombe
 * @param int X de la bombe
 * @param int Y de la bombe
 * @param JsonEncoder* Les infos de l'explosion
 * @return Reussite
 */
boolean bomb_explode(Game*, int, int, int, JsonEncoder*);

/**
 * Explosion des bombes en chaine
 * @param Game* La game de la bombe
 * @param int L'index du joueur à qui appartient la bombe
 * @param int X de la bombe
 * @param int Y de la bombe
 * @param JsonArray* Les infos de l'explosion
 * @param JsonArray* Toutes les cases touchées par l'explosion
 */
void bomb_chain(Game*, int, int, int, JsonArray*, JsonArray*);

/**
 * Determine le spawn des objets (bonus/malus/bombe)
 * @param Game* La game où faire spawn l'objet
 * @param int X
 * @param int Y
 * @param JsonEncoder* Json por recueillir les infos de l'objet ajouté
 * @return 0 Pas d'objet, 1 Bombe, 2 Bonus/Malus
 */
int spawn_object(Game*, int, int, JsonEncoder*);

/**
 * Supprime un joueur d'une partie
 * @param Game* La partie
 * @param int L'index du joueur dans la partie
 */
void remove_player(Game*, int);

/**
 * Stop une partie
 * @param Game* La partie
 */
void stop_game(Game*);

/**
 * Stop toutes les parties
 */
void clean_games();

#endif /* GAME_H */

