/* 
 * File:   file.c
 * Author: Arthur Brandao
 *
 * Created on 23 novembre 2018
 */

#define _DEFAULT_SOURCE

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <dirent.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include "error.h"
#include "str.h"
#include "file.h"

/* --- Fonctions publiques --- */
boolean is_dir(const char* path) {
    struct stat stats;
    if (stat(path, &stats) == ERR) {
        addperror("Impossible d'analyser le fichier");
        return false;
    }
    //Verif existence que le chemin est bien un fichier
    if (S_ISDIR(stats.st_mode)) {
        return true;
    }
    return false;
}

char** file_list(const char* path, int* nb) {
    struct dirent **namelist;
    int scan, nbFile = 0, length;
    char** result;
    //Verif que le chemin est bien un dossier
    if (!is_dir(path)) {
        adderror("Le chemin n'est pas un dossier");
        if (nb != NULL) {
            *nb = ERR;
        }
        return NULL;
    }
    //Scan le repertoire
    scan = scandir(path, &namelist, 0, alphasort);
    if (nbFile < 0) {
        addperror("Impossible de scanner les fichiers");
        if (nb != NULL) {
            *nb = ERR;
        }
        return NULL;
    }
    //Compte le nombre de fichier dans le dossier
    int i = scan;
    while (i--) {
        if (namelist[i]->d_type == DT_REG && namelist[i]->d_name[0] != '.') {
            nbFile++;
        }
    }
    //Si le dossier est vide
    if (nbFile == 0) {
        if (nb != NULL) {
            *nb = 0;
        }
        return NULL;
    }
    //Ajout des resultats dans la variable
    result = malloc(sizeof (char*) * nbFile);
    for (int i = scan - 1, j = 0; i >= 0; i--, j++) {
        //Ne prend que les fichiers non cachés
        if (namelist[i]->d_type == DT_REG && namelist[i]->d_name[0] != '.') {
            length = strlen(namelist[i]->d_name) + 1;
            result[j] = malloc(sizeof (char) * length);
            memset(result[j], 0, length);
            strncpy(result[j], namelist[i]->d_name, length - 1);
        }
    }
    //Retourne chaine + ajout nombre de resultat
    if (nb != NULL) {
        *nb = nbFile;
    }
    return result;
}

char** file_list_all(const char* path, int* nb) {
    struct dirent **namelist;
    int scan, nbFile = 0, length;
    char** result;
    //Verif que le chemin est bien un dossier
    if (!is_dir(path)) {
        adderror("Le chemin n'est pas un dossier");
        if (nb != NULL) {
            *nb = ERR;
        }
        return NULL;
    }
    //Scan le repertoire
    scan = scandir(path, &namelist, 0, alphasort);
    if (nbFile < 0) {
        addperror("Impossible de scanner les fichiers");
        if (nb != NULL) {
            *nb = ERR;
        }
        return NULL;
    }
    //Compte le nombre de fichier dans le dossier
    int i = scan;
    while (i--) {
        if (namelist[i]->d_type == DT_REG) {
            nbFile++;
        }
    }
    //Si le dossier est vide
    if (nbFile == 0) {
        if (nb != NULL) {
            *nb = 0;
        }
        return NULL;
    }
    //Ajout des resultats dans la variable
    result = malloc(sizeof (char*) * nbFile);
    for (int i = scan - 1, j = 0; i >= 0; i--, j++) {
        //Ne prend que les fichiers non cachés
        if (namelist[i]->d_type == DT_REG) {
            length = strlen(namelist[i]->d_name) + 1;
            result[j] = malloc(sizeof (char) * length);
            memset(result[j], 0, length);
            strncpy(result[j], namelist[i]->d_name, length - 1);
        }
    }
    //Retourne chaine + ajout nombre de resultat
    if (nb != NULL) {
        *nb = nbFile;
    }
    return result;
}

char* file_get_content(const char* path) {
    int fd;
    off_t size;
    char* result;
    //Ouverture du fichier
    fd = open(path, O_RDONLY);
    if (fd == ERR) {
        addperror("Impossible d'ouvrir le fichier");
        return NULL;
    }
    //Recup la taille du fichier
    size = lseek(fd, 0L, SEEK_END);
    if (size == ERR) {
        addperror("Impossible de récupèrer la taille du fichier");
        return NULL;
    }
    //Creation de la chaine
    result = malloc(sizeof (char) * (size + 1));
    memset(result, 0, size + 1);
    //Retour au debut + lecture du fichier
    if (lseek(fd, 0L, SEEK_SET) == ERR) {
        addperror("Impossible de revenir au debut du fichier");
        free(result);
        return NULL;
    }
    if (read(fd, result, size) == ERR) {
        addperror("Impossible de lire le contenu du fichier");
        free(result);
        return NULL;
    }
    //Retourne la taille de la chaine
    return result;
}