package rsx;

import org.json.JSONObject;

/**
 * Interface des gestionnaires de requete du serveur
 * @author Arthur Brandao
 */
public interface BomberStudentHandler {
    
    public boolean handle(JSONObject json);
    
}
