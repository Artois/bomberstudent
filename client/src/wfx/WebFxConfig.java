/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wfx;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author Loquicom <contact@loquicom.fr>
 */
public class WebFxConfig {

    private static WebFxConfig instance;

    protected boolean ini = false;
    protected String path = "file/webfx/config.json";
    protected JSONObject json;

    /* --- Singleton --- */
    public static WebFxConfig getInstance() throws IllegalArgumentException {
        if (instance == null) {
            instance = new WebFxConfig();
        }
        return instance;
    }

    public static WebFxConfig getInstance(String path) throws IllegalArgumentException {
        if (instance == null) {
            instance = new WebFxConfig(path);
        }
        return instance;
    }

    private WebFxConfig() {
        //Si un chemin par defaut
        if (this.path != null) {
            this.ini = true;
            //Lecture fichier
            String content = this.fileGetContent(this.path);
            if (content == null) {
                throw new IllegalArgumentException("Fichier introuvable");
            }
            //Lecture JSON
            try {
                this.json = new JSONObject(content);
            } catch (JSONException ex) {
                throw new IllegalArgumentException("JSON invalide");
            }
        }
    }

    private WebFxConfig(String configPath) {
        //Verif fihchier existe
        File f = new File(configPath);
        if (!(f.exists() && f.canRead())) {
            throw new IllegalArgumentException("Fichier introuvable");
        }
        this.ini = true;
        this.path = configPath;
        //Lecture fichier
        String content = this.fileGetContent(this.path);
        if (content == null) {
            throw new IllegalArgumentException("Fichier introuvable");
        }
        //Lecture JSON
        try {
            this.json = new JSONObject(content);
        } catch (JSONException ex) {
            throw new IllegalArgumentException("JSON invalide");
        }
    }

    /* --- Méthodes --- */
    public void setUpWebFx(WebFx wfx) {
        //Si pas de fichier de config
        if(!this.ini){
            return;
        }
        //Titre
        if (this.json.has("title")) {
            wfx.setTitle(this.json.getString("title"));
        }
        //HTML
        if (this.json.has("defaultHtml")) {
            JSONObject jo = this.json.getJSONObject("defaultHtml");
            if (jo.getBoolean("file")) {
                wfx.setHtmlFile(jo.getString("html"));
            } else {
                wfx.setHtml(jo.getString("html"));
            }
        }
        //Icone
        if (this.json.has("icon")) {
            wfx.setIcon(this.json.getString("icon"));
        }
        //Dimension
        if (this.json.has("width") && this.json.has("height")) {
            wfx.setSize(this.json.getInt("width"), this.json.getInt("height"));
        }
        //Zoom
        if(this.json.has("zoom")){
            wfx.setZoom(this.json.getDouble("zoom"));
        }
    }

    /* --- Methodes privées --- */
    private String fileGetContent(String path) {
        //Ouverture fichier
        File f = new File(path);
        if (!(f.exists() && f.canRead())) {
            return null;
        }
        //Lecture fichier
        String fileContent = "";
        try {
            String buf;
            BufferedReader br = new BufferedReader(new FileReader(f));
            while ((buf = br.readLine()) != null) {
                fileContent += buf;
            }
            br.close();
        } catch (FileNotFoundException ex) {
            System.err.println("Le fichier est introuvable : " + ex.getMessage());
            return null;
        } catch (IOException ex) {
            System.err.println("Impossible de lire le fichier : " + ex.getMessage());
            return null;
        }
        return fileContent;
    }

}
