/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wfx;

import bswfx.BomberStudent;
import bswfx.KeyHandler;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.concurrent.atomic.AtomicBoolean;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.image.Image;
import javafx.scene.layout.VBox;
import javafx.scene.web.WebView;
import javafx.stage.Stage;

/**
 *
 * @author Loquicom <contact@loquicom.fr>
 */
public class WebFx extends Application implements Runnable {

    public static Stage stage;
    public static AtomicBoolean ini = new AtomicBoolean(false);
    public static AtomicBoolean wait = new AtomicBoolean(false);

    protected double zoom = 1;
    protected int width = 800;
    protected int height = 800;
    protected String title = "WFX";
    protected String html = "<div style=\"margin-top: 48vh\"><h1 style=\"font-family: Arial\"><div style=\"text-align: center\">Loading...</div></h1></div>";
    protected String icon = null;

    public WebFx() {
        WebFxConfig config = WebFxConfig.getInstance();
        config.setUpWebFx(this);
    }

    public WebFx(String configPath) {
        WebFxConfig config = WebFxConfig.getInstance(configPath);
        config.setUpWebFx(this);
    }

    /* --- Getter/Setter --- */
    public void setZoom(double zoom) {
        this.zoom = zoom;
    }

    public void setSize(int width, int height) {
        this.width = width;
        this.height = height;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setHtml(String html) {
        this.html = html;
    }

    public void setHtmlFile(String htmlFile) {
        String content = this.fileGetContent(htmlFile);
        if (content != null) {
            this.html = content;
        }
    }

    public void setIcon(String path) {
        this.icon = "file:" + path;
    }

    /* --- Méthodes JavaFx --- */
    public void launch() {
        Application.launch();
    }

    public void end() {
        Platform.exit();
    }

    @Override
    public void start(final Stage stage) {
        //Creation element graphiques
        WebView wv = new WebView();
        VBox root = new VBox();

        //Modification taille
        wv.setZoom(this.zoom);
        wv.setMinWidth(this.width);
        wv.setMinHeight(this.height);
        root.setMinWidth(this.width);
        root.setMinHeight(this.height);

        //Recup le stage
        WebFx.stage = stage;

        //Charge page par defaut
        wv.getEngine().loadContent(this.html);

        //Ajoute la Webview à la Vbox
        root.getChildren().add(wv);

        //Modifie le style de la VBox
        /*root.setStyle("-fx-padding: 10;"
                + "-fx-border-style: solid inside;"
                + "-fx-border-width: 2;"
                + "-fx-border-insets: 5;"
                + "-fx-border-radius: 5;"
                + "-fx-border-color: blue;");*/
        //Creation et ajout de la scene
        Scene scene = new Scene(root);
        WebFx.stage.setScene(scene);

        //Parametrage stage
        WebFx.stage.setTitle(title);
        if (this.icon != null) {
            WebFx.stage.getIcons().add(new Image(this.icon));
        }

        //Handler
        WebFx.stage.setOnCloseRequest(event -> {
            BomberStudent.end();
        });
        wv.setOnKeyPressed(keyEvent -> {
            KeyHandler.manage(keyEvent);
        });

        //Affiche la fenetre
        WebFx.stage.show();
        ini.set(true);
    }

    /* --- Méthodes modification fenetre --- */
    public void loadHtml(String html) {
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                VBox v = (VBox) WebFx.stage.getScene().getRoot();
                WebView wv = (WebView) v.getChildren().get(0);
                wv.getEngine().loadContent(html);
            }
        });
    }

    public boolean loadFile(String path) {
        //Recup le contenu du fichier
        String fileContent = this.fileGetContent(path);
        if (fileContent == null) {
            return false;
        }
        //Chargement de l'html
        this.loadHtml(fileContent);
        return true;
    }

    public void alert(String title, String text) {
        wait.set(true);
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                Alert alert = new Alert(AlertType.INFORMATION);
                alert.setTitle(title);
                alert.setHeaderText(null);
                alert.setContentText(text);
                alert.showAndWait();
                WebFx.wait.set(false);
            }
        });
    }
    
    /* --- Methode Runneable --- */
    @Override
    public void run() {
        this.launch();
    }

    /* --- Methodes privées --- */
    private String fileGetContent(String path) {
        //Ouverture fichier
        File f = new File(path);
        if (!(f.exists() && f.canRead())) {
            return null;
        }
        //Lecture fichier
        String fileContent = "";
        try {
            String buf;
            BufferedReader br = new BufferedReader(new FileReader(f));
            while ((buf = br.readLine()) != null) {
                fileContent += buf;
            }
            br.close();
        } catch (FileNotFoundException ex) {
            System.err.println("Le fichier est introuvable : " + ex.getMessage());
            return null;
        } catch (IOException ex) {
            System.err.println("Impossible de lire le fichier : " + ex.getMessage());
            return null;
        }
        return fileContent;
    }

}
