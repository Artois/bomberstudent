package bs;

import bswfx.BomberStudent;
import bswfx.ScreenManager;

/**
 * Gestion du menu principale
 */
public class Menu {
    
    public static final int CREATE = 0;
    public static final int JOIN = 1;
    public static final int QUIT = 2;
    
    protected int selected = 0;
    
    public Menu(){
        ScreenManager.setScreen(ScreenManager.MENU_SCREEN);
    }
    
    public int getAction(){
        return this.selected;
    }
    
    public void up(){
        if(this.selected == 0){
            this.selected = 3;
        }
        this.selected--;
    }
    
    public void down(){
        if(this.selected == 2){
            this.selected = -1;
        }
        this.selected++;
    }
    
    public String toHtml(){
        String html = "<!DOCTYPE html><html><head><link rel=\"stylesheet\" type=\"text/css\" href=\"file:" + System.getProperty("user.dir") + "/file/css/menu.css\"></head><body>";
        html += "<button id=\"id0\" disabled>Créer</button>";
        html += "<button id=\"id1\" disabled>Rejoindre</button>";
        html += "<button id=\"id2\" disabled>Quitter</button>";
        html += "</body></html>";
        html = html.replaceAll("id" + this.selected, "select");
        return html;
    }
    
    public void show(){
        BomberStudent.wfx.loadHtml(this.toHtml());
    }
    
}
