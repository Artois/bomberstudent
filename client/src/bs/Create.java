package bs;

import bswfx.BomberStudent;
import bswfx.ScreenManager;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 * Gestion du menu de création de partie
 */
public class Create {

    public static final int NAME_MODE = 0;
    public static final int CHOOSE_MODE = 1;

    protected int mode = NAME_MODE;
    protected String name = "";
    protected JSONArray maps;
    protected int selected = 0;

    public Create() {
        ScreenManager.setScreen(ScreenManager.CREATE_SCREEN);
    }

    public int getMode() {
        return this.mode;
    }

    public void changeMode(int mode) {
        if (mode == CHOOSE_MODE) {
            BomberStudent.bsc.send("GET", "game/list");
            JSONObject res = BomberStudent.bsc.receive();
            this.maps = res.getJSONArray("maps");
        }
        this.mode = mode;
    }

    public void generateName(KeyEvent ke) {
        if (ke.getCode() == KeyCode.BACK_SPACE) {
            if (this.name.length() > 0) {
                this.name = this.name.substring(0, this.name.length() - 1);
            }
        } else {
            this.name += ke.getText();
        }
    }

    public void up() {
        if (this.selected == 0) {
            this.selected = this.maps.length();
        }
        this.selected--;
    }

    public void down() {
        if (this.selected == this.maps.length() - 1) {
            this.selected = -1;
        }
        this.selected++;
    }

    public void createGame() {
        JSONObject param = new JSONObject();
        param.put("name", this.name);
        param.put("map", this.maps.getString(this.selected));
        BomberStudent.bsc.send("POST", "game/create", param);
        JSONObject res = BomberStudent.bsc.receive();
        if (res.getInt("status") != 201) {
            System.err.println("Impossible de créer la partie");
            BomberStudent.end();
        }
        //Creation game
        BomberStudent.game = new Game(res);
        ScreenManager.setScreen(ScreenManager.GAME_SCREEN);
    }

    public String toHtml() {
        if (this.mode == NAME_MODE) {
            return "<div style=\"margin-top: 48vh\"><h1 style=\"font-family: Arial\"><div style=\"text-align: center\">Entrez un nom :<br>" + this.name + "</div></h1></div>";
        } else {
            String html = "<!DOCTYPE html><html><head><link rel=\"stylesheet\" type=\"text/css\" href=\"file:" + System.getProperty("user.dir") + "/file/css/menu.css\"></head><body>";
            for (int i = 0; i < this.maps.length(); i++) {
                html += "<button id=\"id" + i + "\" disabled>" + this.maps.getString(i) + "</button>";
            }
            html += "</body></html>";
            return html.replaceAll("id" + this.selected, "select");
        }
    }

    public void show() {
        BomberStudent.wfx.loadHtml(this.toHtml());
    }

}
