package bs;

import bswfx.BomberStudent;
import bswfx.ScreenManager;
import java.util.HashMap;
import org.json.JSONArray;
import org.json.JSONObject;
import wfx.WebFx;

/**
 * Gestion des parties
 */
public class Game {

    /**
     * Liste des joueurs avec comme clef leur id
     */
    protected HashMap<Integer, Player> player = new HashMap<>();
    /**
     * L'id du joueur principale
     */
    protected int mainPlayer;
    /**
     * Largeur de la map
     */
    protected int width;
    /**
     * Hauteur de la map
     */
    protected int height;
    /**
     * La map
     */
    protected String map[][];

    /**
     * Création d'une partie à partir du JSON du serveur
     * @param jo Le JSON du serveur
     */
    public Game(JSONObject jo) {
        //Recup la map
        JSONObject joMap = jo.getJSONObject("map");
        this.width = joMap.getInt("width");
        this.height = joMap.getInt("height");
        this.map = this.parseMap(joMap.getString("content"));
        //Creation joueur principale
        JSONObject joPlayer = jo.getJSONObject("player");
        Player p = new Player(joPlayer, jo.getString("startPos"));
        this.player.put(joPlayer.getInt("id"), p);
        this.map[p.getX()][p.getY()] = "" + p.getId();
        this.mainPlayer = p.getId();
        //Creation autre joueurs
        if (jo.has("players")) {
            JSONArray ja = jo.getJSONArray("players");
            for (int i = 0; i < ja.length(); i++) {
                joPlayer = ja.getJSONObject(i);
                int id = joPlayer.getInt("id");
                if (id != this.mainPlayer) {
                    p = new Player(id, joPlayer.getString("pos"));
                    this.player.put(id, p);
                    this.map[p.getX()][p.getY()] = "" + id;
                }
            }
        }
    }

    /* --- Méthodes jeu --- */
    /**
     * Ajoute un joueur dans la partie
     * @param id Id du joueur
     * @param pos Position du joueur
     */
    public void join(int id, String pos) {
        Player p = new Player(id, pos);
        this.player.put(id, p);
        this.map[p.getX()][p.getY()] = "" + id;
    }

    /**
     * Déplace un joueur
     * @param player Id du joueur
     * @param dir La direction du déplacement
     * @return Reussite
     */
    public boolean move(int player, int dir) {
        //Recup le joueur
        Player p = this.player.get(player);
        if (p == null) {
            return false;
        }
        //Calcul nouvelle coord
        int x = p.getX();
        int y = p.getY();
        switch (dir) {
            case Player.UP:
                if (y > 0) {
                    y--;
                }
                break;
            case Player.LEFT:
                if (x > 0) {
                    x--;
                }
                break;
            case Player.DOWN:
                if (y < this.height - 1) {
                    y++;
                }
                break;
            case Player.RIGHT:
                if (x < this.width - 1) {
                    x++;
                }
                break;
            default:
                return false;
        }
        //Regarde si le joueur marche sur un objet
        switch (this.map[x][y]) {
            case "cb":
                this.getObject(Player.CLASSIC);
                break;
            case "mb":
                this.getObject(Player.MINE);
                break;
            case "rb":
                this.getObject(Player.REMOTE);
                break;
            case "bu":
                this.getObject(Player.BOMBUP);
                break;
            case "bd":
                this.getObject(Player.BOMBDOWN);
                break;
            case "f":
                this.getObject(Player.FIREPOWER);
                break;
            case "s":
                this.getObject(Player.SCOOTER);
                break;
            case "bl":
                this.getObject(Player.BROKENLEG);
                break;
            case "i":
                this.getObject(Player.MAJOR);
                break;
            case "lm":
                this.getObject(Player.LIFEMAX);
                break;
            case "lu":
                this.getObject(Player.LIFEUP);
                break;
        }
        //Deplacement
        p.setDir(dir);
        this.map[x][y] = "" + p.getId();
        this.map[p.getX()][p.getY()] = "_";
        //Met à jour le joueur
        p.setX(x);
        p.setY(y);
        return true;
    }
    
    /**
     * Ajoute uene bombe sur le terrain
     * @param type Le type de la bombe
     * @param coord Coordonnées de la bombe
     * @return Reussite
     */
    public boolean newBomb(int type, String coord) {
        //Recup la position
        int pos[] = this.parseCoord(coord);
        //Met la bombe sur la map
        switch (type) {
            case Player.CLASSIC:
                this.map[pos[0]][pos[1]] = "b";
                break;
            case Player.MINE:
                this.map[pos[0]][pos[1]] = "m";
                break;
            case Player.REMOTE:
                this.map[pos[0]][pos[1]] = "r";
                break;
            default:
                return false;
        }
        return true;
    }

    /**
     * Retire une bombe du terrain
     * @param coord Position de la bombe
     */
    public void removeBomb(String coord) {
        int pos[] = this.parseCoord(coord);
        if (this.map[pos[0]][pos[1]].equals("b") || this.map[pos[0]][pos[1]].equals("m") || this.map[pos[0]][pos[1]].equals("r")) {
            this.map[pos[0]][pos[1]] = "_";
        }
    }

    /**
     * Ajoute un objet sur le terrain
     * @param type Le type de l'objet
     * @param coord La position de l'objet
     * @return Reussite
     */
    public boolean newObject(int type, String coord) {
        //Recup la position
        int pos[] = this.parseCoord(coord);
        //Met la bombe sur la map
        switch (type) {
            case Player.CLASSIC:
                this.map[pos[0]][pos[1]] = "cb";
                break;
            case Player.MINE:
                this.map[pos[0]][pos[1]] = "mb";
                break;
            case Player.REMOTE:
                this.map[pos[0]][pos[1]] = "rb";
                break;
            case Player.BOMBUP:
                this.map[pos[0]][pos[1]] = "bu";
                break;
            case Player.BOMBDOWN:
                this.map[pos[0]][pos[1]] = "bd";
                break;
            case Player.FIREPOWER:
                this.map[pos[0]][pos[1]] = "f";
                break;
            case Player.SCOOTER:
                this.map[pos[0]][pos[1]] = "s";
                break;
            case Player.BROKENLEG:
                this.map[pos[0]][pos[1]] = "bl";
                break;
            case Player.MAJOR:
                this.map[pos[0]][pos[1]] = "i";
                break;
            case Player.LIFEUP:
                this.map[pos[0]][pos[1]] = "lu";
                break;
            case Player.LIFEMAX:
                this.map[pos[0]][pos[1]] = "lm";
                break;
            default:
                return false;
        }
        return true;
    }

    /**
     * Récupération d'un objet sur le terrain
     * @param type Le type de l'objet
     * @return Reussite
     */
    public boolean getObject(int type) {
        JSONObject param = new JSONObject();
        //Creation json en fonction type
        switch (type) {
            case Player.CLASSIC:
                param.put("type", "bomb");
                param.put("class", "classic");
                break;
            case Player.MINE:
                param.put("type", "bomb");
                param.put("class", "mine");
                break;
            case Player.REMOTE:
                param.put("type", "bomb");
                param.put("class", "remote");
                break;
            case Player.BOMBUP:
                param.put("type", "bonusMalus");
                param.put("class", "bomb_up");
                break;
            case Player.BOMBDOWN:
                param.put("type", "bonusMalus");
                param.put("class", "bomb_down");
                break;
            case Player.FIREPOWER:
                param.put("type", "bonusMalus");
                param.put("class", "fire_power");
                break;
            case Player.SCOOTER:
                param.put("type", "bonusMalus");
                param.put("class", "scooter");
                break;
            case Player.BROKENLEG:
                param.put("type", "bonusMalus");
                param.put("class", "broken_legs");
                break;
            case Player.MAJOR:
                param.put("type", "bonusMalus");
                param.put("class", "major");
                break;
            case Player.LIFEUP:
                param.put("type", "bonusMalus");
                param.put("class", "life_up");
                break;
            case Player.LIFEMAX:
                param.put("type", "bonusMalus");
                param.put("class", "life_max");
                break;
            default:
                return false;
        }
        //Envoi
        if (!BomberStudent.bsc.send("POST", "object/new", param)) {
            return false;
        }
        //Reception
        JSONObject res = BomberStudent.bsc.receive();
        if (res.getInt("status") != 201) {
            return false;
        }
        //MaJ joueur
        Player p = this.player.get(this.mainPlayer);
        p.setLife(res.getInt("life"));
        p.setMaxLife(res.getInt("maxLife"));
        p.setSpeed(res.getInt("speed"));
        p.setBomb(res.getInt("currentNbClassicBomb"));
        p.setMine(res.getInt("currentNbMine"));
        p.setRemote(res.getInt("currentNbRemoteBomb"));
        p.setMaxBomb(res.getInt("maxNbBomb"));
        //Ajout bonus
        if (type == Player.FIREPOWER) {
            p.setFirepower(p.getFirepower() + 1);
        } else if (type == Player.MAJOR) {
            p.setMajor(true);
        }
        return true;
    }

    /**
     * Met à jour la carte
     * @param mapContent La carte en une ligne
     */
    public void updateMap(String mapContent) {
        String map[][] = this.parseMap(mapContent);
        //Compare les 2 cartes
        for (int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {
                if (this.map[j][i].equals("*")) {
                    this.map[j][i] = map[j][i];
                }
            }
        }
    }

    /**
     * Met à jour la vie
     * @param newLife La nouvelle valeur
     */
    public void updateLife(int newLife) {
        Player p = this.player.get(this.mainPlayer);
        //Si non invincible
        if (!p.isMajor()) {
            if (newLife > 0) {
                p.setLife(newLife);
            } else {
                this.gameOver();
            }
        }
    }

    /**
     * Quitte une partie
     * @param id Id du joueur qui quitte
     * @return Reussite
     */
    public boolean quit(int id) {
        //Recup joueur
        Player p = this.player.get(id);
        if (p == null) {
            return false;
        }
        //Le retire
        this.player.remove(id);
        this.map[p.getX()][p.getY()] = "_";
        return true;
    }
    
    /**
     * Ecran de game over
     */
    public void gameOver(){
        //Quitte la partie
        BomberStudent.bsc.send("POST", "game/quit");
        //Change l'ecran
        ScreenManager.setScreen(ScreenManager.END_SCREEN);
        WebFx.wait.set(true);
        BomberStudent.wfx.loadHtml("<div style=\"margin-top: 48vh\"><h1 style=\"font-family: Arial\"><div style=\"text-align: center\">GAME OVER<br>Press any key...</div></h1></div>");
        BomberStudent.waitWfx();
        BomberStudent.menu = new Menu();
        BomberStudent.menu.show();
    }
    
    /**
     * Affiche les explosions des bombes
     * @param explo JSON avec les coordonnées des explosions
     */
    public void explosion(JSONArray explo){
        int x, y;
        JSONObject pos;
        //Ajout explosion map
        for(int i = 0; i < explo.length(); i++){
            pos = explo.getJSONObject(i);
            x = pos.getInt("x");
            y = pos.getInt("y");
            this.map[x][y] = "e" + this.map[x][y];
        }
        //Timer pour la retirer
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                try{
                    Thread.sleep(1000);
                } catch (Exception ex) {
                    
                }
                BomberStudent.game.removeExplosion();
                BomberStudent.updateWindow();
            }
        });
        t.start();
    }
    
    /**
     * Supprime les explosions du terrain
     */
    public void removeExplosion(){
        for (int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {
                while(this.map[j][i].charAt(0) == 'e'){
                    this.map[j][i] = this.map[j][i].substring(1);
                }
            }
        }
    }

    /* --- Méthodes affichage --- */
    /**
     * Genere l'HTML pour l'affichage
     * @return 
     */
    public String toHtml() {
        Player pl = this.player.get(this.mainPlayer);
        String html = "<!DOCTYPE html><html><head><link rel=\"stylesheet\" type=\"text/css\" href=\"file:" + System.getProperty("user.dir") + "/file/css/map.css\"></head><body><div id=\"game\">";
        html += "<div id=\"hud\">" + pl.getLife() + "/" + pl.getMaxLife() + "</div>";
        html += "<table><tbody>";
        for (int i = 0; i < height; i++) {
            html += "<tr>";
            for (int j = 0; j < width; j++) {
                html += "<td><img src=\"file:" + System.getProperty("user.dir") + "/";
                switch (this.map[j][i].charAt(0)) {
                    case 'e':
                        html += "file/sprite/explosion.png";
                        break;
                    case '_':
                        html += "file/sprite/sol.png";
                        break;
                    case '-':
                        html += "file/sprite/limite.png";
                        break;
                    case '*':
                        html += "file/sprite/mur.png";
                        break;
                    case 'b':
                        switch (this.map[j][i]) {
                            case "bu":
                                html += "file/sprite/bomb_up.png";
                                break;
                            case "bd":
                                html += "file/sprite/bomb_down.png";
                                break;
                            case "bl":
                                html += "file/sprite/broken_leg.png";
                                break;
                            default:
                                html += "file/sprite/bomb.png";
                                break;
                        }
                        break;
                    case 'm':
                        switch (this.map[j][i]) {
                            case "mb":
                                html += "file/sprite/mine_bomb.png";
                                break;
                            default:
                                html += "file/sprite/mine.png";
                                break;
                        }
                        break;
                    case 'r':
                        switch (this.map[j][i]) {
                            case "rb":
                                html += "file/sprite/remote_bomb.png";
                                break;
                            default:
                                html += "file/sprite/remote.png";
                                break;
                        }
                        break;
                    case 'c':
                        html += "file/sprite/classic_bomb.png";
                        break;
                    case 'f':
                        html += "file/sprite/fire_power.png";
                        break;
                    case 's':
                        html += "file/sprite/scooter.png";
                        break;
                    case 'i':
                        html += "file/sprite/major.png";
                        break;
                    case 'l':
                        switch (this.map[j][i]) {
                            case "lu":
                                html += "file/sprite/life_up.png";
                                break;
                            default:
                                html += "file/sprite/life_max.png";
                                break;
                        }
                        break;
                    case '0':
                    case '1':
                    case '2':
                    case '3':
                    case '4':
                    case '5':
                    case '6':
                    case '7':
                    case '8':
                    case '9':
                        Player p = this.player.get(Integer.parseInt(this.map[j][i]));
                        if (p.isMain()) {
                            html += "file/sprite/player_";
                            if(p.isMajor()){
                                html += "m";
                            }
                            switch(p.dir){
                                case Player.UP:
                                    html += "u.png";
                                    break;
                                case Player.LEFT:
                                    html += "l.png";
                                    break;
                                case Player.DOWN:
                                    html += "d.png";
                                    break;
                                case Player.RIGHT:
                                    html += "r.png";
                                    break;
                            }
                        } else {
                            html += "file/sprite/other_";
                            switch(p.dir){
                                case Player.UP:
                                    html += "u.png";
                                    break;
                                case Player.LEFT:
                                    html += "l.png";
                                    break;
                                case Player.DOWN:
                                    html += "d.png";
                                    break;
                                case Player.RIGHT:
                                    html += "r.png";
                                    break;
                            }
                        }
                        break;
                }
                html += "\"></td>";
            }
            html += "</tr>";
        }
        html += "</tbody></table></div></body></html>";
        return html;
    }

    /**
     * Affiche la map dans le terminal
     */
    public void showMap() {
        for (int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {
                System.out.print(this.map[j][i]);
            }
            System.out.println("");
        }
    }

    /* --- Getter/Setter --- */
    public Player getPlayer(int index) {
        return this.player.get(index);
    }

    public int getMainPlayerId() {
        return this.mainPlayer;
    }

    public int getHeight() {
        return this.height;
    }

    public int getWidth() {
        return this.width;
    }

    /* --- Methodes privée --- */
    /**
     * Parse la map 1D en 2D
     * @param mapContent
     * @return 
     */
    private String[][] parseMap(String mapContent) {
        String[][] map = new String[this.width][this.height];
        for (int i = 0; i < this.height; i++) {
            for (int j = 0; j < this.width; j++) {
                map[j][i] = "" + mapContent.charAt((i * this.width) + j);
            }
        }
        return map;
    }

    /**
     * Parse les coordonnées
     * @param coord Coordonnées
     * @return 0 => X, 1 => Y
     */
    private int[] parseCoord(String coord) {
        int res[] = new int[2];
        //Cherche le separateur
        int pos = 0;
        while (pos < coord.length()) {
            if (coord.charAt(pos) == ',') {
                break;
            }
            pos++;
        }
        //Si pas de separateur
        if (pos == coord.length()) {
            return null;
        }
        //Recup les 2 string + extraction int
        String substr1 = coord.substring(0, pos);
        String substr2 = coord.substring(pos + 1);
        res[0] = Integer.parseInt(substr1);
        res[1] = Integer.parseInt(substr2);
        return res;
    }

}
