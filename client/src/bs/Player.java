package bs;

import bswfx.BomberStudent;
import org.json.JSONObject;

/**
 * Gestion joueur
 */
public class Player {
    
    //Deplacement
    public static final int UP = 0;
    public static final int LEFT = 1; 
    public static final int DOWN = 2;
    public static final int RIGHT = 3;
    //Type de bombe
    public static final int CLASSIC = 0;
    public static final int MINE = 1;
    public static final int REMOTE = 2;
    //Objet
    public static final int BOMBUP = 3;
    public static final int BOMBDOWN = 4;
    public static final int FIREPOWER = 5;
    public static final int SCOOTER = 6;
    public static final int BROKENLEG = 7;
    public static final int MAJOR = 8;
    public static final int LIFEUP = 9;
    public static final int LIFEMAX = 10;
    
    protected boolean main;
    protected int id;
    protected int dir;
    protected int x;
    protected int y;
    protected int life;
    protected int maxLife;
    protected int speed;
    protected int maxBomb;
    protected int bomb;
    protected int mine;
    protected int remote;
    protected int firepower;
    protected boolean major;
    
    /**
     * Création d'un joueur non principal (sur un autre client)
     * @param id L'id du joueur
     * @param pos La position de départ
     */
    public Player(int id, String pos){
        int coord[] = parseCoord(pos);
        //SetUp les valeurs
        this.main = false;
        this.id = id;
        this.dir = RIGHT;
        this.x = coord[0];
        this.y = coord[1];
    }
    
    /**
     * Création d'un joueur principal (sur ce client)
     * @param jo JSON décrivant le joueur
     * @param pos La position de départ
     */
    public Player(JSONObject jo, String pos){
        int coord[] = parseCoord(pos);
        //SetUp les valeurs
        this.main = true;
        this.id = jo.getInt("id");
        this.dir = RIGHT;
        this.x = coord[0];
        this.y = coord[1];
        this.life = jo.getInt("life");
        this.maxLife = jo.getInt("maxLife");
        this.speed = jo.getInt("speed");
        this.maxBomb = jo.getInt("maxNbBomb");
        this.bomb = jo.getInt("currentNbClassicBomb");
        this.mine = jo.getInt("currentNbMine");
        this.remote = jo.getInt("currentNbRemoteBomb");
        this.firepower = 0;
        this.major = false;
    }
    
    /* --- Méthodes --- */
    /**
     * Déplace le joueur sur le serveur
     * @param dir La direction
     * @return Reussite
     */
    public boolean move(int dir){
        JSONObject jo = new JSONObject();
        switch(dir){
            case UP:
                jo.put("move", "up");
                return BomberStudent.bsc.send("POST", "player/move", jo);
            case LEFT:
                jo.put("move", "left");
                return BomberStudent.bsc.send("POST", "player/move", jo);      
            case DOWN:
                jo.put("move", "down");
                return BomberStudent.bsc.send("POST", "player/move", jo);
            case RIGHT:
                jo.put("move", "right");
                return BomberStudent.bsc.send("POST", "player/move", jo);
            default:
                return false;
        }
    }
    
    /**
     * Pose une bombe
     * @param type Le type de la bombe
     * @return Reussite
     */
    public boolean attack(int type){
        JSONObject jo = new JSONObject();
        //Calcul position
        int x = this.x;
        int y = this.y;
        switch(this.dir){
            case UP:
                if(y == 0){
                    return false;
                }
                y--;
                break;
            case LEFT:
                if(x == 0){
                    return false;
                }
                x--;
                break;   
            case DOWN:
                if(y == BomberStudent.game.getHeight() - 1){
                    return false;
                }
                y++;
                break;
            case RIGHT:
                if(x == BomberStudent.game.getWidth() - 1){
                    return false;
                }
                x++;
                break;
            default:
                return false;
        }
        jo.put("pos", x + "," + y);
        //Regarde le type
        switch(type){
            case CLASSIC:
                jo.put("class", "classic");
                break;
            case MINE:
                jo.put("class", "mine");
                break;
            case REMOTE:
                jo.put("class", "remote");
                break;
            default:
                return false;
        }
        //Si il a un bonus
        jo.put("bonus", this.firepower > 0);
        //Envoi
        if(!BomberStudent.bsc.send("POST", "attack/bomb", jo)){
            return false;
        }
        JSONObject res = BomberStudent.bsc.receive();
        if(res.getInt("status") == 201){
            if(type == MINE){
                BomberStudent.game.newBomb(MINE, x + "," + y);
                BomberStudent.updateWindow();
            }
            return true;
        }
        return false;
    }

    /* --- Getter/Setter --- */
    
    public boolean isMain() {
        return main;
    }

    public int getId() {
        return id;
    }
    
    public int getDir(){
        return dir;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getLife() {
        return life;
    }

    public int getMaxLife() {
        return maxLife;
    }

    public int getSpeed() {
        return speed;
    }

    public int getMaxBomb() {
        return maxBomb;
    }

    public int getBomb() {
        return bomb;
    }

    public int getMine() {
        return mine;
    }

    public int getRemote() {
        return remote;
    }

    public int getFirepower() {
        return firepower;
    }

    public boolean isMajor() {
        return major;
    }

    public void setMain(boolean main) {
        this.main = main;
    }

    public void setId(int id) {
        this.id = id;
    }
    
    public void setDir(int dir){
        this.dir = dir;
    }

    public void setX(int x) {
        this.x = x;
    }

    public void setY(int y) {
        this.y = y;
    }

    public void setLife(int life) {
        this.life = life;
    }

    public void setMaxLife(int maxLife) {
        this.maxLife = maxLife;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    public void setMaxBomb(int maxBomb) {
        this.maxBomb = maxBomb;
    }

    public void setBomb(int bomb) {
        this.bomb = bomb;
    }

    public void setMine(int mine) {
        this.mine = mine;
    }

    public void setRemote(int remote) {
        this.remote = remote;
    }

    public void setFirepower(int firepower) {
        this.firepower = firepower;
    }

    public void setMajor(boolean major) {
        this.major = major;
    }
    
    /* --- Methodes privées --- */
    /**
     * Parse les coordonnées
     * @param coord Coordonnées
     * @return 0 => X, 1 => Y
     */
    private int[] parseCoord(String coord){
        int res[] = new int[2];
        //Cherche le separateur
        int pos = 0;
        while(pos < coord.length()){
            if(coord.charAt(pos) == ','){
                break;
            }
            pos++;
        }
        //Si pas de separateur
        if(pos == coord.length()){
            return null;
        }
        //Recup les 2 string + extraction int
        String substr1 = coord.substring(0, pos);
        String substr2 = coord.substring(pos + 1);
        res[0] = Integer.parseInt(substr1);
        res[1] = Integer.parseInt(substr2);
        return res;
    }
    
}
