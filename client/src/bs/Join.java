package bs;

import bswfx.ScreenManager;
import org.json.JSONArray;
import bswfx.BomberStudent;
import org.json.JSONObject;

/**
 * Gestion du menu pour rejoindre une partie
 */
public class Join {

    protected JSONArray games;
    protected int selected = 0;

    public Join() {
        ScreenManager.setScreen(ScreenManager.JOIN_SCREEN);
        BomberStudent.bsc.send("GET", "game/list");
        JSONObject res = BomberStudent.bsc.receive();
        if (!res.has("games")) {
            BomberStudent.menu = new Menu();
            BomberStudent.menu.show();
        } else {
            this.games = res.getJSONArray("games");
            if (this.games.length() == 0) {
                BomberStudent.menu = new Menu();
                BomberStudent.menu.show();
            }
        }
    }

    public void up() {
        if (this.selected == 0) {
            this.selected = this.games.length();
        }
        this.selected--;
    }

    public void down() {
        if (this.selected == this.games.length() - 1) {
            this.selected = -1;
        }
        this.selected++;
    }

    public void connect() {
        JSONObject game = this.games.getJSONObject(this.selected);
        JSONObject param = new JSONObject();
        param.put("name", game.getString("name"));
        BomberStudent.bsc.send("POST", "game/join", param);
        JSONObject res = BomberStudent.bsc.receive();
        if (res.getInt("status") != 201) {
            System.err.println("Impossible de rejoindre la partie");
            BomberStudent.end();
        }
        //Creation game
        BomberStudent.game = new Game(res);
        ScreenManager.setScreen(ScreenManager.GAME_SCREEN);
    }

    public String toHtml() {
        String html = "<!DOCTYPE html><html><head><link rel=\"stylesheet\" type=\"text/css\" href=\"file:" + System.getProperty("user.dir") + "/file/css/menu.css\"></head><body>";
        for (int i = 0; i < this.games.length(); i++) {
            JSONObject jo = this.games.getJSONObject(i);
            html += "<button id=\"id" + i + "\" disabled>" + jo.getString("name") + " - " + jo.getString("map") + " (" + jo.getInt("nbPlayer") + "j.)</button>";
        }
        html += "</body></html>";
        return html.replaceAll("id" + this.selected, "select");
    }

    public void show() {
        BomberStudent.wfx.loadHtml(this.toHtml());
    }

}
