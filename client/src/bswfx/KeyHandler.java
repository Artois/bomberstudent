package bswfx;

import bs.Create;
import bs.Join;
import bs.Menu;
import bs.Player;
import javafx.scene.input.KeyEvent;
import wfx.WebFx;

/**
 * Gestion des entrées claviers
 */
public class KeyHandler {

    /**
     * Repartition en fonction de l'ecran afficher
     * @param ke L'event KeyPressed
     */
    public static void manage(KeyEvent ke) {
        switch (ScreenManager.getScreen()) {
            case ScreenManager.GAME_SCREEN:
                gameEvent(ke);
                break;
            case ScreenManager.END_SCREEN:
                WebFx.wait.set(false);
                break;
            case ScreenManager.MENU_SCREEN:
                menuEvent(ke);
                break;
            case ScreenManager.CREATE_SCREEN:
                createEvent(ke);
                break;
            case ScreenManager.JOIN_SCREEN:
                joinEvent(ke);
        }
    }

    /**
     * Gestion des touches pour la partie
     * @param ke L'event KeyPressed
     */
    protected static void gameEvent(KeyEvent ke) {
        //Recup le joueur principale
        Player p = BomberStudent.game.getPlayer(BomberStudent.game.getMainPlayerId());
        switch (ke.getCode()) {
            case Z:
                p.move(Player.UP);
                break;
            case Q:
                p.move(Player.LEFT);
                break;
            case S:
                p.move(Player.DOWN);
                break;
            case D:
                p.move(Player.RIGHT);
                break;
            case A:
                p.attack(Player.CLASSIC);
                break;
            case E:
                p.attack(Player.MINE);
                break;
            case R:
                p.attack(Player.REMOTE);
                break;
            case F:
                BomberStudent.bsc.send("POST", "attack/remote/go");
                break;
            case I:
                BomberStudent.game.getPlayer(BomberStudent.game.getMainPlayerId()).setDir(Player.UP);
                BomberStudent.updateWindow();
                break;
            case J:
                BomberStudent.game.getPlayer(BomberStudent.game.getMainPlayerId()).setDir(Player.LEFT);
                BomberStudent.updateWindow();
                break;
            case K:
                BomberStudent.game.getPlayer(BomberStudent.game.getMainPlayerId()).setDir(Player.DOWN);
                BomberStudent.updateWindow();
                break;
            case L:
                BomberStudent.game.getPlayer(BomberStudent.game.getMainPlayerId()).setDir(Player.RIGHT);
                BomberStudent.updateWindow();
                break;
            case ESCAPE:
                BomberStudent.bsc.send("POST", "game/quit");
                BomberStudent.menu = new Menu();
                BomberStudent.menu.show();
                break;
        }
    }

    /**
     * Gestion des touches pour le menu principale
     * @param ke L'event KeyPressed
     */
    protected static void menuEvent(KeyEvent ke) {
        switch (ke.getCode()) {
            case Z:
                BomberStudent.menu.up();
                BomberStudent.menu.show();
                break;
            case S:
                BomberStudent.menu.down();
                BomberStudent.menu.show();
                break;
            case ENTER:
                switch (BomberStudent.menu.getAction()) {
                    case Menu.QUIT:
                        BomberStudent.end();
                        break;
                    case Menu.CREATE:
                        BomberStudent.create = new Create();
                        BomberStudent.create.show();
                        break;
                    case Menu.JOIN:
                        BomberStudent.join = new Join();
                        BomberStudent.join.show();
                        break;
                }
                break;
        }
    }

    /**
     * Gestion des touches pour la création de partie
     * @param ke L'event KeyPressed
     */
    public static void createEvent(KeyEvent ke) {
        if (BomberStudent.create.getMode() == Create.NAME_MODE) {
            switch (ke.getCode()) {
                case ENTER:
                    BomberStudent.create.changeMode(Create.CHOOSE_MODE);
                    break;
                default:
                    BomberStudent.create.generateName(ke);
                    break;
            }
            BomberStudent.create.show();
        } else {
            switch (ke.getCode()) {
                case Z:
                    BomberStudent.create.up();
                    BomberStudent.create.show();
                    break;
                case S:
                    BomberStudent.create.down();
                    BomberStudent.create.show();
                    break;
                case ENTER:
                    BomberStudent.create.createGame();
                    BomberStudent.updateWindow();
                    break;
            }
        }
    }

    /**
     * Gestion de touche pour rejoindre une partie
     * @param ke L'event KeyPressed
     */
    public static void joinEvent(KeyEvent ke) {
        switch (ke.getCode()) {
            case Z:
                BomberStudent.join.up();
                BomberStudent.join.show();
                break;
            case S:
                BomberStudent.join.down();
                BomberStudent.join.show();
                break;
            case ENTER:
                BomberStudent.join.connect();
                BomberStudent.updateWindow();
                break;
        }
    }

}
