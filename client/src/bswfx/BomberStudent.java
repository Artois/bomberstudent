package bswfx;

import bs.Create;
import bswfx.handler.*;
import bs.Game;
import bs.Join;
import bs.Menu;
import org.json.JSONObject;
import rsx.BomberStudentClient;
import wfx.WebFx;

/**
 * Class principal
 */
public class BomberStudent {
    
    /**
     * Affichage
     */
    public static WebFx wfx = new WebFx();
    /**
     * Connexion avec le serveur
     */
    public static BomberStudentClient bsc = new BomberStudentClient();
    /**
     * La partie actuelle
     */
    public static Game game;
    /**
     * Le menu principale
     */
    public static Menu menu;
    /**
     * Le menu de création de partie
     */
    public static Create create;
    /**
     * Le menu pour rejoindre une partie
     */
    public static Join join;

    public static void main(String[] args) {
        //Change passage de ligne du systeme
        System.setProperty("line.separator", "\n");
        
        //Variables
        JSONObject param;
        JSONObject res;
        
        //Lancement Thread affichage
        Thread twfx = new Thread(wfx);
        twfx.start();
        
        //Ajout handler
        bsc.addHandler("server/end", new HandlerServerEnd());
        bsc.addHandler("player/position/update", new HandlerPlayerMove());
        bsc.addHandler("game/newplayer", new HandlerPlayerJoin());
        bsc.addHandler("game/quit", new HandlerPlayerQuit());
        bsc.addHandler("attack/newbomb", new HandlerAttackBomb());
        bsc.addHandler("attack/explose", new HandlerAttackExplose());
        bsc.addHandler("attack/affect", new HandlerAttackAffect());
        bsc.addHandler("player/major/end", new HandlerMajorEnd());
        
        //Recherche serveur actif
        int nbRes = bsc.findServer();
        if (nbRes == 0) {
            wfx.alert("Erreur", "Aucun serveur trouvé");
            waitWfx();
            wfx.end();
            System.exit(0);
        }
        System.out.println("Nombre serveur : " + nbRes);
        
        //Connexion serveur et recup liste des games
        bsc.selectServer(0);
        
        //Verif affichage ok
        while(!WebFx.ini.get());
        
        //Affichage menu
        menu = new Menu();
        menu.show();
    }
    
    public static void updateWindow(){
        wfx.loadHtml(game.toHtml());
    }
    
    public static void end(){
        wfx.end();
        bsc.close();
        System.exit(0);
    }
    
    public static void waitWfx(){
        while(wfx.wait.get()){
            try{
                Thread.sleep(1000);
            } catch(Exception ex) {
                
            }
        }
    }

}
