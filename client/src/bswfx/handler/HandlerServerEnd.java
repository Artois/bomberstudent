package bswfx.handler;

import org.json.JSONObject;
import rsx.BomberStudentHandler;
import bswfx.BomberStudent;

/**
 * Handler server/end
 */
public class HandlerServerEnd implements BomberStudentHandler{

    @Override
    public boolean handle(JSONObject json) {
        BomberStudent.wfx.alert("Déconnexion", "La connexion avec le serveur a été perdue");
        BomberStudent.waitWfx();
        BomberStudent.end();
        return true;
    }
    
}
