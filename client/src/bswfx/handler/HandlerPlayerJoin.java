package bswfx.handler;

import org.json.JSONObject;
import rsx.BomberStudentHandler;
import bswfx.BomberStudent;

/**
 * Handler game/newplayer
 */
public class HandlerPlayerJoin implements BomberStudentHandler{

    @Override
    public boolean handle(JSONObject json) {
        //Verif param
        if(!( json.has("id") && json.has("pos") )){
            return false;
        }
        //Ajout
        BomberStudent.game.join(json.getInt("id"), json.getString("pos"));
        BomberStudent.updateWindow();
        return true;
    }
    
}
