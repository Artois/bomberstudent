package bswfx.handler;

import org.json.JSONObject;
import rsx.BomberStudentHandler;
import bswfx.BomberStudent;

/**
 * Handler game/quit
 */
public class HandlerPlayerQuit implements BomberStudentHandler{

    @Override
    public boolean handle(JSONObject json) {
        //Verif param
        if(!( json.has("player") )){
            return false;
        }
        //Retire le joueur
        if(BomberStudent.game.quit(json.getInt("player"))){
            BomberStudent.updateWindow();
            return true;
        }
        return false;
    }
    
}
