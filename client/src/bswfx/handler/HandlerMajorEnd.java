package bswfx.handler;

import bs.Player;
import org.json.JSONObject;
import rsx.BomberStudentHandler;
import bswfx.BomberStudent;

/**
 * Handler player/major/end
 */
public class HandlerMajorEnd implements BomberStudentHandler{

    @Override
    public boolean handle(JSONObject json) {
        Player p = BomberStudent.game.getPlayer(BomberStudent.game.getMainPlayerId());
        p.setMajor(false);
        BomberStudent.updateWindow();
        return true;
    }
    
}
