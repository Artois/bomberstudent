
package bswfx.handler;

import org.json.JSONObject;
import rsx.BomberStudentHandler;
import bswfx.BomberStudent;

/**
 * Handler attack/affect
 */
public class HandlerAttackAffect implements BomberStudentHandler{

    @Override
    public boolean handle(JSONObject json) {
        //Verif param
        if(!( json.has("life") )){
            return false;
        }
        //MaJ la vie
        BomberStudent.game.updateLife(json.getInt("life"));
        return true;
    }
    
}
