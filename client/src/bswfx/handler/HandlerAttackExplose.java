package bswfx.handler;

import bs.Player;
import org.json.JSONObject;
import rsx.BomberStudentHandler;
import bswfx.BomberStudent;
import org.json.JSONArray;

/**
 * Handler attack/explose
 */
public class HandlerAttackExplose implements BomberStudentHandler {

    @Override
    public boolean handle(JSONObject json) {
        //Verif param
        if (!(json.has("pos") && json.has("map") && json.has("explosion") )) {
            return false;
        }
        //Retire les bombes explosées
        BomberStudent.game.removeBomb(json.getString("pos"));
        if (json.has("chain")) {
            JSONArray ja = json.getJSONArray("chain");
            for (int i = 0; i < ja.length(); i++) {
                JSONObject jo = ja.getJSONObject(i);
                BomberStudent.game.removeBomb(jo.getString("pos"));
            }
        }
        //Met à jour la map
        BomberStudent.game.updateMap(json.getString("map"));
        //Ajoute les bombes / bonus
        if (json.has("bomb")) {
            JSONArray ja = json.getJSONArray("bomb");
            for (int i = 0; i < ja.length(); i++) {
                JSONObject jo = ja.getJSONObject(i);
                switch (jo.getString("type")) {
                    case "classic":
                        BomberStudent.game.newObject(Player.CLASSIC, jo.getString("pos"));
                        break;
                    case "mine":
                        BomberStudent.game.newObject(Player.MINE, jo.getString("pos"));
                        break;
                    case "remote":
                        BomberStudent.game.newObject(Player.REMOTE, jo.getString("pos"));
                        break;
                }
            }
        }
        if (json.has("bonusMalus")) {
            JSONArray ja = json.getJSONArray("bonusMalus");
            for (int i = 0; i < ja.length(); i++) {
                JSONObject jo = ja.getJSONObject(i);
                switch (jo.getString("type")) {
                    case "bomb_up":
                        BomberStudent.game.newObject(Player.BOMBUP, jo.getString("pos"));
                        break;
                    case "bomb_down":
                        BomberStudent.game.newObject(Player.BOMBDOWN, jo.getString("pos"));
                        break;
                    case "fire_power":
                        BomberStudent.game.newObject(Player.FIREPOWER, jo.getString("pos"));
                        break;
                    case "scooter":
                        BomberStudent.game.newObject(Player.SCOOTER, jo.getString("pos"));
                        break;
                    case "broken_legs":
                        BomberStudent.game.newObject(Player.BROKENLEG, jo.getString("pos"));
                        break;
                    case "major":
                        BomberStudent.game.newObject(Player.MAJOR, jo.getString("pos"));
                        break;
                    case "life_up":
                        BomberStudent.game.newObject(Player.LIFEUP, jo.getString("pos"));
                        break;
                    case "life_max":
                        BomberStudent.game.newObject(Player.LIFEMAX, jo.getString("pos"));
                        break;
                }
            }
        }
        //Gére l'affichage des explosions
        BomberStudent.game.explosion(json.getJSONArray("explosion"));
        //MaJ affichage
        BomberStudent.updateWindow();
        return true;
    }

}
