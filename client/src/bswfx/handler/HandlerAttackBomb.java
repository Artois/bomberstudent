package bswfx.handler;

import bs.Player;
import org.json.JSONObject;
import rsx.BomberStudentHandler;
import bswfx.BomberStudent;

/**
 * Handler attack/newbomb
 */
public class HandlerAttackBomb implements BomberStudentHandler{

    @Override
    public boolean handle(JSONObject json) {
        //Verif json
        if(!( json.has("pos") && json.has("class") )){
            return false;
        }
        //Recup le type
        int type;
        switch(json.getString("class")){
            case "classic":
                type = Player.CLASSIC;
                break;
            case "mine":
                return true;
            case "remote":
                type = Player.REMOTE;
                break;
            default:
                return false;
        }
        //Ajoute sur la map
        if(BomberStudent.game.newBomb(type, json.getString("pos"))){
            BomberStudent.updateWindow();
            return true;
        }
        return false;
    }
    
}
