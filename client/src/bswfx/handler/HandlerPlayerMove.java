package bswfx.handler;

import bs.Player;
import org.json.JSONObject;
import rsx.BomberStudentHandler;
import bswfx.BomberStudent;

/**
 * Handler player/position/update
 */
public class HandlerPlayerMove implements BomberStudentHandler{

    @Override
    public boolean handle(JSONObject json) {
        //Verif param
        if(!( json.has("player") && json.has("dir") )){
            return false;
        }
        //Lecture direction
        int dir;
        switch(json.getString("dir")){
            case "up":
                dir = Player.UP;
                break;
            case "left":
                dir = Player.LEFT;
                break;
            case "down":
                dir = Player.DOWN;
                break;
            case "right":
                dir = Player.RIGHT;
                break;
            default:
                return false;
        }
        //Deplacement
        if(BomberStudent.game.move(json.getInt("player"), dir)){
            BomberStudent.updateWindow();
            return true;
        }
        return false;
    }
    
}
