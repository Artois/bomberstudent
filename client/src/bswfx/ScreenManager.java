package bswfx;

/**
 * Gestion de l'ecran actuellement affiché
 */
public class ScreenManager {
    
    public static final int LOADING_SCREEN = 0;
    public static final int GAME_SCREEN = 1;
    public static final int END_SCREEN = 2;
    public static final int MENU_SCREEN = 3;
    public static final int CREATE_SCREEN = 4;
    public static final int JOIN_SCREEN = 5;
    
    protected static int screen = LOADING_SCREEN;
    
    public static int getScreen(){
        return ScreenManager.screen;
    }
    
    public static void setScreen(int screen){
        ScreenManager.screen = screen;
    }
    
}
